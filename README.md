# FauDok-UI



## Description
Web Application responsible to deliver PDF scans to students of FAU Erlangen. Emails received from ZFS Munchen are processed and the metadata is extracted and saved in the DB. A print job is sent to a printer, based on the "Signatur" received. An worker get the print, scans the first page (barcode with information about the scan) and the rest of the requested pages from the book. Afterwards an email with a link is sent to the user, where the scan can be downlaoded as PDF. The link will point to a directory with a name generated from an UUID (PDF only accessible if the directory name is properly given in the URL). If the scan can't complete (legal rights etc), the job will be reported in the syste, and an email is sent to user. 
Information regarding the old DOD can be found under: <code>S:\it\dod</code>. The current system will be documented under <code>S:\it\FAUdok-in-progress</code>.

![FauDok Workflow](https://gitlab.rrze.fau.de/ro23bivi/faudok/-/blob/main/FauDok.png?raw=true)

## Installation
TO DO

## Usage
TO DO

## Authors and acknowledgment
Stelica Valianos, Martin Scholz, Martin Thoma

## License
MIT License

## Project status
Active

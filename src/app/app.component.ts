import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { Alert } from './alert/alert';
import { LoadingService } from './services/loading.service';
import { AlertService } from './services/alert.service';
import { MAT_SNACK_BAR_DATA, MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  public alerts: Alert[] = [];
  public loading: boolean;
  title = 'Faudok';

  constructor(
    private loadingService: LoadingService, 
    private alertService: AlertService,
    private _snackBar: MatSnackBar,
  ) {
    this.loading = false;
  }

  //init global loading and alert service
  ngOnInit() {
    this.subscriptions.push(
      this.loadingService.isLoading.subscribe(isLoading => {
        this.loading = isLoading;
      })
    );
    this.subscriptions.push(
      this.alertService.alerts.subscribe(alert => {
        let config = new MatSnackBarConfig();
        config.panelClass = [alert.type.toString()];
        config.duration = 3000;
        console.log(JSON.stringify(config));
        this._snackBar.open(alert.text, '', config);
        //this.alerts.push(alert);
      })
    );
  }


  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}

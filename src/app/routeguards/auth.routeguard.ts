import { TokenService } from './../services/token.service';
//import { TokenCookieService } from '../services/token-cookie.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot,Router } from '@angular/router';

@Injectable()
export class AuthActivateRouteGuard implements CanActivate {
    
    constructor(private router: Router, 
      //private tokenCookieService: TokenCookieService,
      private tokenService: TokenService,
    ){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
          this.tokenService.redirectUrl = state.url;
          if (this.tokenService.isTokenNotValid()) {
            this.router.navigate(['login']);
            return false;
          }
          return true;
    };

}
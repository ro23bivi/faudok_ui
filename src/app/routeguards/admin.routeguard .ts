import { ManagementService } from 'src/openapi/generated/services/management.service';
import { Injectable, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot,Router } from '@angular/router';
import { TokenService } from '../services/token.service';

@Injectable()
export class AdminActivateRouteGuard implements CanActivate {
    
    role: String;

    constructor(
        private router: Router, 
        private managementService: ManagementService,
        private tokenService: TokenService
    ){}
    
    private getRole() {
        if (this.tokenService.isLoggedIn()) {
            this.role = this.tokenService.userRoles.length > 0
                ? this.tokenService.userRoles[0] : '';
            if (this.role == '') {
                this.managementService.getRole().subscribe({
                    next: (res) => {
                        this.role = res;
                    }
                });
            }
        }
    }

    canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot){
        this.getRole();
        console.log(this.role);
        return this.role == 'ROLE_ADMIN' ? true : false;
    }

}
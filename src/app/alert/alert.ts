import { AlertType } from "./alert-type.enum";

export class Alert {
    text: string;
    type: AlertType;
    duration: number;
  
    constructor(text = '', type = AlertType.SUCCESS, duration = 3000) {
      this.text = text;
      this.type = type;
      this.duration = duration;
    }
  }
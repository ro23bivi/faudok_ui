import { Injectable } from '@angular/core';
import { HttpInterceptor,HttpRequest,HttpHandler,HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import {Router} from '@angular/router';
import {tap} from 'rxjs/operators';

@Injectable()
export class XhrInterceptor implements HttpInterceptor {


  constructor(private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    let httpHeaders = new HttpHeaders();
    //get saved user details if stored from prev authentication, and use Basic Auth to login in Server (Server will create and save a cookie with a token, for future requests)
    if(sessionStorage.getItem('userdetails')){
 //     this.user = JSON.parse(sessionStorage.getItem('userdetails')!);
    }
    //if userdetails availabe, send email and password for basic auth
    // if(this.user && this.user.password && this.user.email){
    //   httpHeaders = httpHeaders.append('Authorization', 'Basic ' + window.btoa(this.user.email + ':' + this.user.password));
    // }
    //if alrdy login and already have XSRF token received in cookie session, send it via Header with every request-> 
    //we use the name of header as define in Java: X-XSRF-TOKEN
    let xsrf = sessionStorage.getItem('XSRF-TOKEN');
    if(xsrf){
      httpHeaders = httpHeaders.append('X-XSRF-TOKEN', xsrf);  
    }
    httpHeaders = httpHeaders.append('X-Requested-With', 'XMLHttpRequest');
    //headers are immutable, so copy old headers and add new headers
    const xhr = req.clone({
      headers: httpHeaders
    });
    return next.handle(xhr).pipe(tap(
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status !== 401) {
            return;
          }
          //if success login, redirect to orders
          this.router.navigate(['orders']);
        }
      }));
  }
}
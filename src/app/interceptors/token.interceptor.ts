//import { TokenCookieService } from '../services/token-cookie.service';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    //private tokenCookieService: TokenCookieService,
    private tokenService: TokenService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    //skip intercepting request for login/register/resertPassword as they dont need a token to send to server
    if (request.url.includes(`/login`)) {
      return next.handle(request);
    }
    if (request.url.includes(`/register`)) {
      return next.handle(request);
    }
    if (request.url.includes(`/activate-account`)) {
      return next.handle(request);
    }

    const token = this.tokenService.token;
    if(token) {
      const authRequest = request.clone({
        headers: new HttpHeaders({
          Authorization: `Bearer ${token}`
        })
      });
      return next.handle(authRequest);
    }
    return next.handle(request);
  }

}

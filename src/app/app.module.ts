import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//import { JwtModule } from '@auth0/angular-jwt';

import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { MatChipsModule } from '@angular/material/chips';
import { MatDividerModule } from '@angular/material/divider';


import { CodeInputModule } from 'angular-code-input';  //to provide an interface to insert the code for verification on registration
import { QRCodeModule } from 'angularx-qrcode';    //to generate QR code
import { PdfViewerModule } from 'ng2-pdf-viewer';  //to view pdf
import { NgxLoadingModule } from "ngx-loading";  //for loading spinner
import { TranslateHttpLoader } from '@ngx-translate/http-loader'; //for translation

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ContactComponent } from './components/contact/contact.component';
import { HomeComponent } from './components/home/home.component';
import { XhrInterceptor } from './interceptors/app.request.interceptor';
import { AuthActivateRouteGuard } from './routeguards/auth.routeguard';
import { AdminActivateRouteGuard } from './routeguards/admin.routeguard ';
import { FaqComponent } from './components/faq/faq.component';
import { OrderHomeComponent } from './components/order-home/order-home.component';
import { ActivateComponent } from './components/activate-account/activate.component';
import { FooterComponent } from './components/footer/footer.component';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { ManageOrderComponent } from './components/order-home/manage-order/manage-order.component';
import { OrderDetailsResolverService } from './components/order-home/order-resolver.service';
import { LoginComponent } from './components/login/login.component';
import { MyAvailableOrdersComponent } from './components/order-home/my-available-orders/my-available-orders.component';
import { ChairsAvailableOrdersComponent } from './components/order-home/chairs-available-orders/chairs-available-orders.component';
import { AllAvailableOrdersComponent } from './components/order-home/all-available-orders/all-available-orders.component';
import { MyArchivedOrdersComponent } from './components/order-home/my-archived-orders/my-archived-orders.component';
import { ChairsArchivedOrdersComponent } from './components/order-home/chairs-archived-orders/chairs-archived-orders.component';
import { AllArchivedOrdersComponent } from './components/order-home/all-archived-orders/all-archived-orders.component';
import { ChairsDeletedOrdersComponent } from './components/order-home/chairs-deleted-orders/chairs-deleted-orders.component';
import { AllDeletedOrdersComponent } from './components/order-home/all-deleted-orders/all-deleted-orders.component';
// import { AlertSnackbarComponent } from './alert/alert-snackbar/alert-snackbar.component';
import { AlertService } from './services/alert.service';
import { SearchComponent } from './components/search/search.component';
import { UserInfoComponent } from './components/user/user-info/user-info.component';
import { UserRegistrationComponent } from './components/user/user-registration/user-registration.component';
import { OrderScanViewComponent } from './components/order-home/order-scan-view/order-scan-view.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { OrderRejectDialogComponent } from './components/order-home/order-reject-dialog/order-reject-dialog.component';
import { OrderCardArchivedComponent } from './components/order-home/order-card-archived/order-card-archived.component';
import { OrderCardAvailableComponent } from './components/order-home/order-card-available/order-card-available.component';
import { OrderCardDeletedComponent } from './components/order-home/order-card-deleted/order-card-deleted.component';
import { MoveOrderDialogComponent } from './components/order-home/move-order-dialog/move-order-dialog.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ForgotPasswordValidationComponent } from './components/forgot-password-validation/forgot-password-validation.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    ContactComponent,
    HomeComponent,
    FaqComponent,
    OrderHomeComponent,
    ActivateComponent,
    FooterComponent,
    ManageOrderComponent,
    MyAvailableOrdersComponent,
    ChairsAvailableOrdersComponent,
    AllAvailableOrdersComponent,
    MyArchivedOrdersComponent,
    ChairsArchivedOrdersComponent,
    AllArchivedOrdersComponent,
    ChairsDeletedOrdersComponent,
    AllDeletedOrdersComponent,
    SearchComponent,
    UserInfoComponent,
    UserRegistrationComponent,
    OrderScanViewComponent,
    ConfirmationDialogComponent,
    OrderRejectDialogComponent,
    OrderCardArchivedComponent,
    OrderCardAvailableComponent,
    OrderCardDeletedComponent,
    MoveOrderDialogComponent,
    ForgotPasswordComponent,
    ForgotPasswordValidationComponent,
    // AlertSnackbarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxLoadingModule.forRoot({}),
    MatExpansionModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatTableModule,
    MatTabsModule,
    MatPaginatorModule,
    MatSortModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatCardModule,
    MatDialogModule,
    MatSidenavModule,
    MatListModule,
    MatSelectModule,
    MatInputModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatChipsModule,
    MatDividerModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'XSRF-TOKEN',
      headerName: 'X-XSRF-TOKEN',
    }),
    BrowserAnimationsModule,
    TranslateModule.forRoot(
      {
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }
    ),
    CodeInputModule,
    QRCodeModule,
    PdfViewerModule,
    // JwtModule.forRoot({
    //   config: {
    //     tokenGetter: tokenGetter,
    //     allowedDomains: ["example.com"],
    //     disallowedRoutes: ["http://example.com/examplebadroute/"],
    //   },
    // }),
  ],
  providers: [
    {
      provide : HTTP_INTERCEPTORS,
      useClass : XhrInterceptor,
      multi : true
    },
    {
      provide : HTTP_INTERCEPTORS,
      useClass : TokenInterceptor,
      multi : true
    },
    AuthActivateRouteGuard,
    AdminActivateRouteGuard,
    OrderDetailsResolverService,
    AlertService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

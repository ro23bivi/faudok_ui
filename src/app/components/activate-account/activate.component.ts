import { AuthenticationService } from '../../../openapi/generated/services/authentication.service';
import { Router } from '@angular/router';
import { Component, OnDestroy } from '@angular/core';
import {skipUntil, Subscription} from 'rxjs';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertService } from 'src/app/services/alert.service';
import { AlertType } from 'src/app/alert/alert-type.enum';

@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.css']
})
export class ActivateComponent implements OnDestroy {

  message: string = '';
  isValid: boolean = true;
  submitted: boolean = false;
  private subscriptions: Subscription[] = [];


  constructor(private router: Router,
              private authenticationService: AuthenticationService,
              private loadingService: LoadingService,
              private alertService: AlertService,
  ) { }


  private confirmAccount(token: string) {
    this.loadingService.isLoading.next(true);
    this.subscriptions.push(
      this.authenticationService.confirm({ token }).subscribe({
        next: () => {
          this.loadingService.isLoading.next(false);
          this.alertService.showAlert('Your account has been successfully activated.', AlertType.SUCCESS);
          this.message = 'Your account has been successfully activated.\nNow you can proceed to login';
          this.submitted = true;
          this.isValid = true;
        },
        error: () => {
          this.loadingService.isLoading.next(false);
          this.alertService.showAlert('Something went wrong. Please try again.', AlertType.ERROR);
          this.message = 'Token has been expired or invalid';
          this.submitted = true;
          this.isValid = false;
        }
      })
    );
  }

  redirectToLogin() {
    this.router.navigate(['login']);
  }

  onCodeCompleted(token: string) {
    this.confirmAccount(token);
  }


  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }


}

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { TokenService } from 'src/app/services/token.service';
import { AuthenticationService } from 'src/openapi/generated/services';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {

  email = '';
  subscriptions: Subscription[] = [];

  constructor(
    private authService: AuthenticationService,
    private tokenService: TokenService,
    private router: Router,
    private alertService: AlertService,
    private loadingService: LoadingService,
    ) { }

  onSubmit() {
    if (this.email) {
      this.loadingService.isLoading.next(true);
      this.subscriptions.push(
        this.authService.resetPasswordRequestEmail({ body: { email: this.email } }).subscribe({
          next: (res) => {
            this.loadingService.isLoading.next(false);
            this.alertService.showAlert('Reset password sent. Please check your email.', AlertType.SUCCESS );
            this.router.navigate(['login']);
          },
          error: (err) => {
            this.alertService.showAlert('Reset password failed. Please try again.', AlertType.ERROR );
            this.loadingService.isLoading.next(false);
          }
        })
      );
    }
  }

}

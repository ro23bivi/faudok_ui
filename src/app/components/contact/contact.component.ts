import { AlertService } from './../../services/alert.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { SendContactRequest$Params } from 'src/openapi/generated/fn/contact/send-contact-request';
import { ContactRequestDto } from 'src/openapi/generated/models/contact-request-dto';
import { ContactService } from 'src/openapi/generated/services';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  model: ContactRequestDto;
  
  constructor(
    private contactService: ContactService,
    private alertService: AlertService,
  ) {
  }

  ngOnInit() {
    this.model = {
      email: '',
      message: '',
      subject: '',
    };
  }

  sendMessage(contactForm: NgForm) {
    const params: SendContactRequest$Params = {
      body: this.model
    }
    this.contactService.sendContactRequest(params).subscribe(
      responseData => {
        this.alertService.showAlert('Your message has been sent successfully.', AlertType.SUCCESS);
        contactForm.resetForm();
      },
      error => {
        this.alertService.showAlert('Something went wrong. Please try again.', AlertType.ERROR);
      }
    );
  }

}

import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AuthenticationService } from 'src/openapi/generated/services';

@Component({
  selector: 'app-forgot-password-validation',
  templateUrl: './forgot-password-validation.component.html',
  styleUrls: ['./forgot-password-validation.component.css']
})
export class ForgotPasswordValidationComponent implements OnDestroy {

  password = '';
  token = '';
  message: string = '';
  isValid: boolean = true;
  submitted: boolean = false;
  private subscriptions: Subscription[] = [];


  constructor(private router: Router,
              private authenticationService: AuthenticationService,
              private loadingService: LoadingService,
              private alertService: AlertService,
  ) { }


  private confirmCode(token: string) {
    this.token = token;
    this.onPasswordSubmit();
  }

  onPasswordSubmit() {
    if(this.token && this.password){
      this.loadingService.isLoading.next(true);
      this.subscriptions.push(
        this.authenticationService.resetPasswordHandler({ body: { code: this.token, password: this.password }}).subscribe({
          next: () => {
            this.loadingService.isLoading.next(false);
            this.alertService.showAlert('Password reset successful.', AlertType.SUCCESS);
            this.submitted = true;
            this.isValid = true;
            this.router.navigate(['login']);
          },
          error: () => {
            this.loadingService.isLoading.next(false);
            this.alertService.showAlert('Something went wrong. Please try again.', AlertType.ERROR);
            this.submitted = true;
            this.isValid = false;
          }
        })
      );
   }
  }

  redirectToLogin() {
    this.router.navigate(['login']);
  }

  onCodeCompleted(token: string) {
    this.confirmCode(token);
  }


  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}

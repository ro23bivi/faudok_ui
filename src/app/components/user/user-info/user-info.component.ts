import { LoadingService } from './../../../services/loading.service';
import { AlertService } from './../../../services/alert.service';
import { ManagementService } from 'src/openapi/generated/services/management.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FaudokUserInfoResponseDto } from 'src/openapi/generated/models';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit, OnDestroy {


  user: FaudokUserInfoResponseDto;
  userForm: FormGroup;
  private subscriptions: Subscription[] = [];


  constructor(
    private managementService: ManagementService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private router: Router,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.initFB();
    this.getUserInfo();
  }

  initFB() {  
    this.userForm = this.formBuilder.group({
      name: '',// new FormControl([{value: '', disabled: false}, Validators.required]),
      email: '', //new FormControl([{value: '', disabled: true}, Validators.required]),
      faculty: '', // new FormControl([{value: '', disabled: true}, Validators.required]), 
      role: '', // new FormControl([{value: '', disabled: true}, Validators.required]),
    });
  }

  getUserInfo() {
    this.loadingService.isLoading.next(true);
    this.subscriptions.push(
      this.managementService.getAccountDetails().subscribe({
        next: (res) => {
          this.loadingService.isLoading.next(false);
          this.user = res;
          //this.initFB();
          this.userForm.patchValue(this.user);
          Object.keys(this.userForm.controls).forEach(controlName => {
            // we dont allow edit for clientDeliveryEmail and clientDeliveryName
            if(controlName == 'name') {
              this.userForm.get(controlName).enable();
            } else {
              this.userForm.get(controlName).disable();
            }
          });
        },
        error: (err) => {
          this.loadingService.isLoading.next(false);
          this.alertService.showAlert('Failed to load user details', AlertType.ERROR);
        }
      })
    );
  }

  onSubmit() {
    if (this.userForm.valid) {
      const name = this.userForm.get('name').value;
      this.loadingService.isLoading.next(true);
      this.subscriptions.push(
        this.managementService.updateFaudokUser1({ body: { name: name } }).subscribe({
          next: (res) => {
            this.loadingService.isLoading.next(false);
            this.alertService.showAlert('Update success.', AlertType.SUCCESS );
            this.router.navigate(['orders']);
          },
          error: (err) => {
            this.loadingService.isLoading.next(false);
            this.alertService.showAlert('Update failed. Please try again.', AlertType.ERROR );
          }
        })
      );
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ManagementService } from 'src/openapi/generated/services';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit, OnDestroy {

  public registerForm!: FormGroup;
  faculties = ['HB', 'TNZB', 'WSZB', 'EZB','JURA', 'NBG_FINDELGASSE', 'HB_DOD_SCAN_ZIMMER', 'SISIS_GRUPPE_TEST', 'TB_THEOLOGIE_KOCHSTRASSE', 'ERL_BISMARCKSTRASSE'];
  roles = ["ROLE_ADMIN", "ROLE_USER"];
  registerRequest = { email: '', name: '', password: '', faculty: '', role: '' };
  private subscriptions: Subscription[] = [];


  constructor(
    private fb: FormBuilder, 
    private managementService: ManagementService,
    private router: Router,
    private loadingService: LoadingService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.registerForm = this.fb.group({
      name: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(62)]],
      confirmPassword: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      faculty: ['', Validators.required],
      role: ['', Validators.required],
    }, { validator: this.passwordMatchValidator });
  }

  passwordMatchValidator(control: FormGroup): { [s: string]: boolean } {
    if (control.get('password').value !== control.get('confirmPassword').value) {
      return { passwordsDontMatch: true };
    }
    return null;
  }

  onSubmit() {
    if (this.registerForm.valid) {
      this.loadingService.isLoading.next(true);
      const {confirmPassword, ...rest} = this.registerForm.value;
      Object.assign(this.registerRequest, rest);

      this.subscriptions.push(
        this.managementService.register({ body: this.registerRequest }).subscribe({
          next: (res) => {
            this.loadingService.isLoading.next(false);
            this.alertService.showAlert('Registration request sent. Please check your email for account details.', AlertType.SUCCESS);
            this.router.navigate(['orders']);
          },
          error: (err) => {
            this.loadingService.isLoading.next(false);
            this.alertService.showAlert('Something went wrong. Please try again.', AlertType.ERROR);
          }
        })
      );
      this.registerForm.reset();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  //Angular sends user and pass from form to java, where we config httpsecurity to allow access to /user only if authenticated,
//so java will try basic auth with provided username and pasw
// validateUser(loginForm: NgForm) {
//   this.loginService.validateLoginDetails(this.model).subscribe(
//     //from the service we will receive all the response data aka body, 
//     //headers, cookies etc from java becasue in service we use { observe: 'response',withCredentials: true }
//     responseData => {
//       this.model = <any> responseData.body;
//       //console.log(JSON.stringify(this.model));
//       this.model.authStatus = 'AUTH';
//       //set a cookie with user details
//       window.sessionStorage.setItem("userdetails", JSON.stringify(this.model));
//       let xsrf = getCookie('XSRF-TOKEN')!;  //get cookie from the response from java
//       //set cookie with XSRF-TOKEN received from Java -> we need to send this cookie with XSRF token with every request-> add it in interceptor
//       window.sessionStorage.setItem("XSRF-TOKEN", xsrf);
//       this.router.navigate(['dashboard']);
//     });
  //}

}

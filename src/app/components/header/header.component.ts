import { TokenService } from 'src/app/services/token.service';
import { OrderService } from './../../../openapi/generated/services/order.service';
//import { TokenCookieService } from 'src/app/services/token-cookie.service';
import { ManagementService } from 'src/openapi/generated/services/management.service';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FaudokResponsePageOrderResponseDto, OrderResponseDto } from 'src/openapi/generated/models';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { MatSidenav } from '@angular/material/sidenav';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  
  username: string = '';
  isUserLoggedIn: boolean = false;
  searchedOrders: OrderResponseDto[] = [];
  selectedLanguage = 'de';
  opened = false;
  isDesktop: boolean = true;

  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;  //to monitor the sidenav
  isMobile= true;
  isCollapsed = true;
  private subscriptions: Subscription[] = [];


  constructor(
    private translateService: TranslateService, 
    private managementService: ManagementService,
    private router: Router,
    private alertService: AlertService,
    private loadingService: LoadingService,
    //private tokenCookieService: TokenCookieService,
    private tokenService: TokenService,
    private orderService: OrderService,
    private observer: BreakpointObserver,
  ) {
    this.translateService.setDefaultLang(this.selectedLanguage);
  }

  toggleMenu() {
    if(this.isMobile){
      this.sidenav.toggle();
      this.isCollapsed = false; // On mobile, the menu can never be collapsed
    } else {
      this.sidenav.open(); // On desktop/tablet, the menu can never be fully closed
      this.isCollapsed = !this.isCollapsed;
    }
  }

  ngOnInit() {
    //check screen size to change layout of navbar menu
    this.observer.observe(['(max-width: 800px)']).subscribe((screenSize) => {
      if(screenSize.matches){
        this.isMobile = true;
      } else {
        this.isMobile = false;
      }
    });
    // if(sessionStorage.getItem('userdetails')){
    //   this.user = JSON.parse(sessionStorage.getItem('userdetails')!);
    // }
    this.username = this.tokenService.getUsername();
    this.subscriptions.push(
        this.tokenService.isLoggedInSubject.subscribe(
          (isLoggedIn: boolean) => {
            this.isUserLoggedIn = isLoggedIn;
          }
        )
    );
  }

  viewDetails() {
    this.router.navigate(['/account']);
  }

  registerUser() {
    this.router.navigate(['/register']);
  }

  switchLanguage(language) {
    this.selectedLanguage = language;
    // this.selectedLanguage = language;
    this.translateService.use(this.selectedLanguage);
  }


  
  logout(): void {
    this.loadingService.isLoading.next(true);
    this.tokenService.removeToken();
    this.tokenService.isLoggedInSubject.next(false);
    this.isUserLoggedIn = false;
    this.router.navigateByUrl('/login');
    this.loadingService.isLoading.next(false);
    this.alertService.showAlert('You have been successfully logged out.', AlertType.SUCCESS);
  }

  onSearchOrder(event) {
    // console.log(event);
    // const signature = event;
    const signature = event.target.value;
    this.subscriptions.push(this.orderService.getChairsOrderBySignature(signature).subscribe(
      (response: FaudokResponsePageOrderResponseDto) => {
        console.log(response);
        this.searchedOrders = response.content;
      },
      error => {
        console.log(error);
        return this.searchedOrders = [];
      }
    ));
  }
  
  savePdf(pdf: File): void {
    this.subscriptions.push(
      // this.orderService.uploadPdf(pdf).subscribe(
      //   response => {
      //       console.log(response);
      //   },
      //   error => {
      //     console.log(error);
      //   }
      // )
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe);
  }

}

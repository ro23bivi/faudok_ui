import { AdminOrderService } from './../../../openapi/generated/services/admin-order.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { TokenService } from 'src/app/services/token.service';
import { GetAnyOrderBySignature$Params } from 'src/openapi/generated/fn/admin-order/get-any-order-by-signature';
import { FaudokResponsePageOrderResponseDto, OrderResponseDto } from 'src/openapi/generated/models';
import { OrderService } from 'src/openapi/generated/services';
import { ManagementService } from 'src/openapi/generated/services/management.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy{

  searchCriteria: string = 'id';
  searchKeyword: string = '';
  role = '';
  ordersPage: FaudokResponsePageOrderResponseDto;
  subscriptions: Subscription[] = [];

  constructor(private tokenService: TokenService,
    private managementService: ManagementService,
    private orderService: OrderService,
    private adminOrderService: AdminOrderService,
  ) { }

  onSearch() {
    console.log(this.searchCriteria, this.searchKeyword);
    //if role is admin, allow the search in all orders, otherwise allow it only in the user's team
    //search by title / date / id
    if(this.searchCriteria == 'id' && this.searchKeyword != '') {
      if(this.role == 'ROLE_ADMIN') {
        this.adminOrderService.getAnyOrderById({'orderId': this.searchKeyword}).subscribe(
          order => this.ordersPage = {content: [order], totalElements: 1, totalPages: 1, 
            pageNo: 0, pageSize: 1, first: true, last: true} 
        );
      } else if(this.role == 'ROLE_USER') {
        this.orderService.getChairsOrderById({'orderId': this.searchKeyword}).subscribe(
          order => this.ordersPage = {content: [order], totalElements: 1, totalPages: 1, 
            pageNo: 0, pageSize: 1, first: true, last: true} 
        );
      }
      this.searchKeyword = '';
    }
    if(this.searchCriteria == 'signature' && this.searchKeyword != '') {
      if(this.role == 'ROLE_ADMIN') {
        this.adminOrderService.getAnyOrderBySignature({'orderSignature': this.searchKeyword}).subscribe(
          orders => { this.ordersPage = orders }
        );
      } else if(this.role == 'ROLE_USER') {
        this.orderService.getChairsOrderBySignature({'orderSignature': this.searchKeyword}).subscribe(
          orders => { this.ordersPage = orders }
        );
      }
    }
    if(this.searchCriteria == 'email' && this.searchKeyword != '') {
      if(this.role == 'ROLE_ADMIN') {
        this.adminOrderService.getAnyOrdersByClientEmail({'clientEmail': this.searchKeyword}).subscribe(
          orders => { this.ordersPage = orders }
        );
      } else if(this.role == 'ROLE_USER') {
        this.orderService.getChairsOrderByClientEmail({'clientEmail': this.searchKeyword}).subscribe(
          orders => { this.ordersPage = orders }
        );
      }
    }
    
    //display orders in list
  }

  ngOnInit(): void {
    if(this.tokenService.isLoggedInSubject.getValue()){ 
      this.role =  this.tokenService.userRoles.length > 0 
          ? this.tokenService.userRoles[0] : '';
      if(this.role == '') {
        this.subscriptions.push(
          this.managementService.getRole().subscribe({
            next: (res) => {
              this.role = res;
            }
          })
        );
      }
    }
    console.log(this.role);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}

import { environment } from 'src/environments/environment';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
// import { TokenCookieService } from 'src/app/services/token-cookie.service';
import { TokenService } from 'src/app/services/token.service';
import { LoginRequestDto } from 'src/openapi/generated/models';
import { AuthenticationService } from 'src/openapi/generated/services/authentication.service';
import { AppConstants } from 'src/app/constants/app.constants';
import { Subscription } from 'rxjs/internal/Subscription';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {


// //Angular sends user and pass from form to java, where we config httpsecurity to allow access to /user only if authenticated,
// //so java will try basic auth with provided username and pasw
//   validateUser(loginForm: NgForm) {
//     this.loginService.validateLoginDetails(this.model).subscribe(
//       //from the service we will receive all the response data aka body, 
//       //headers, cookies etc from java becasue in service we use { observe: 'response } + using { withCredentials: true }, we send with request coockies/local storage/session storage related to localhost:4200
//       responseData => {
//         this.model = <any> responseData.body;
//         //console.log(JSON.stringify(this.model));
//         this.model.authStatus = 'AUTH';
//         //set a cookie with user details
//         window.sessionStorage.setItem("userdetails", JSON.stringify(this.model));
//         let xsrf = getCookie('XSRF-TOKEN')!;  //get cookie from the response from java
//         //set cookie with XSRF-TOKEN received from Java -> we need to send this cookie with XSRF token with every request-> add it in interceptor
//         window.sessionStorage.setItem("XSRF-TOKEN", xsrf);
//         this.router.navigate(['dashboard']);
//       });

//   }

loginForm: FormGroup;
private subscriptions: Subscription[] = [];


constructor(
  private loadingService: LoadingService,
  private alertService: AlertService,
  private router: Router,
  private fb: FormBuilder,
  private authService: AuthenticationService,
  //TODO use TokenCookieService as is more secure
  // private tokencookieService: TokenCookieService,
  private tokenService: TokenService,
) {
}

ngOnInit(): void {
  this.loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(62)]],
  });
  if (this.tokenService.isTokenValid()) {
    //if already loggedin, go directly to requested page
    if (this.tokenService.redirectUrl) {
      this.router.navigateByUrl(this.tokenService.redirectUrl);
    } else {
      //or if no specific page, go directly to orders
      this.router.navigateByUrl('/orders');
    }
  } else {
    this.router.navigateByUrl('/login');
  }
}


onSubmit() {
  if (this.loginForm.valid) {
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;
    this.loadingService.isLoading.next(true);
    this.subscriptions.push(
      this.authService.login({ body: { email: email, password: password } }).subscribe({
        next: (res) => {
          this.loadingService.isLoading.next(false);
          this.alertService.showAlert('Login success.', AlertType.SUCCESS );
          //this.tokencookieService.storeToken(res.token as string);
          this.tokenService.token = res.token as string;
          this.tokenService.isLoggedInSubject.next(true);
          this.router.navigate(['orders']);
        },
        error: (err) => {
          this.alertService.showAlert('Login failed. Please try again.', AlertType.ERROR );
          this.loadingService.isLoading.next(false);
          this.tokenService.isLoggedInSubject.next(false);
        }
      })
    );
  }
}

onForgotPassword(): void {
  this.router.navigate(['forgot-password']);
}

ngOnDestroy() {
  this.subscriptions.forEach(sub => sub.unsubscribe());
}

}

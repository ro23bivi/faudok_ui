import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { GetChairsDeletedOrders$Params } from 'src/openapi/generated/fn/admin-order/get-chairs-deleted-orders';
import { FaudokResponsePageOrderResponseDto, OrderResponseDto } from 'src/openapi/generated/models';
import { AdminOrderService } from 'src/openapi/generated/services';

@Component({
  selector: 'app-chairs-deleted-orders',
  templateUrl: './chairs-deleted-orders.component.html',
  styleUrls: ['./chairs-deleted-orders.component.css']
})
export class ChairsDeletedOrdersComponent  implements OnInit {

  
  ordersResponse: FaudokResponsePageOrderResponseDto = {};
  page = 0;
  size = 5;
  sortBy = 'doneDate';
  sortDir = 'desc';
  pages: any = [];

  constructor(
    private loadingService: LoadingService,
    private alertService: AlertService,
    private adminOrderService: AdminOrderService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.findChairsDeletedOrders();
  }

  private findChairsDeletedOrders() {
    this.loadingService.isLoading.next(true);
    let params: GetChairsDeletedOrders$Params = {
      pageNo: this.page,
      pageSize: this.size,
      sortBy: this.sortBy,
      sortDir: this.sortDir
    };
    this.adminOrderService.getChairsDeletedOrders(params).subscribe({
      next: (res) => {
        this.loadingService.isLoading.next(false);
        this.ordersResponse = res;
        //get total nr of pages
        this.pages = Array(this.ordersResponse.totalPages)
            .fill(0)
            .map((x, i) => i);
      }, 
      error: (err) => {
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert("Something went wrong. Please try again later.", AlertType.ERROR );
      }
    });
  }

  viewOrder(order: OrderResponseDto) {
    this.router.navigate(['orders','manage', order.id]);
  }

  //completly delete the order
  purgeOrder(order: OrderResponseDto) {
    this.loadingService.isLoading.next(true);
    this.adminOrderService.purgeOrder({ id: order.id }).subscribe({
      next: (res) => {
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert("Order purged successfully", AlertType.SUCCESS );
        this.findChairsDeletedOrders();
      },
      error: (err) => {
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert("Something went wrong. Please try again later.", AlertType.ERROR );
      }
    })
  }
  
  gotToPage(page: number) {
    this.page = page;
    this.findChairsDeletedOrders();
  }

  goToFirstPage() {
    this.page = 0;
    this.findChairsDeletedOrders();
  }

  goToPreviousPage() {
    this.page --;
    this.findChairsDeletedOrders();
  }

  goToLastPage() {
    this.page = this.ordersResponse.totalPages as number - 1;
    this.findChairsDeletedOrders();
  }

  goToNextPage() {
    this.page++;
    this.findChairsDeletedOrders();
  }

  get isLastPage() {
    return this.page === this.ordersResponse.totalPages as number - 1;
  }
}
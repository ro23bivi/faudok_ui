import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChairsDeletedOrdersComponent } from './chairs-deleted-orders.component';

describe('ChairsDeletedOrdersComponent', () => {
  let component: ChairsDeletedOrdersComponent;
  let fixture: ComponentFixture<ChairsDeletedOrdersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChairsDeletedOrdersComponent]
    });
    fixture = TestBed.createComponent(ChairsDeletedOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoveOrderDialogComponent } from './move-order-dialog.component';

describe('MoveOrderDialogComponent', () => {
  let component: MoveOrderDialogComponent;
  let fixture: ComponentFixture<MoveOrderDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MoveOrderDialogComponent]
    });
    fixture = TestBed.createComponent(MoveOrderDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

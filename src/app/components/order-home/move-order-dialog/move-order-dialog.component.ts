import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-move-order-dialog',
  templateUrl: './move-order-dialog.component.html',
  styleUrls: ['./move-order-dialog.component.css']
})
export class MoveOrderDialogComponent {

  moveOrderForm: FormGroup;
  chairs: string[] =  ['HB', 'TNZB', 'WSZB', 'EZB','JURA', 'NBG_FINDELGASSE', 'HB_DOD_SCAN_ZIMMER', 'SISIS_GRUPPE_TEST', 'TB_THEOLOGIE_KOCHSTRASSE', 'ERL_BISMARCKSTRASSE'];

  constructor(
    public dialogRef: MatDialogRef<MoveOrderDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder
  ) {
    this.moveOrderForm = this.fb.group({
      selectedChair: ['', Validators.required]
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    if (this.moveOrderForm.valid) {
      this.dialogRef.close(this.moveOrderForm.value);
    }
  }
  
}

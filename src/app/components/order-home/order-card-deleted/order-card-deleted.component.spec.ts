import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderCardDeletedComponent } from './order-card-deleted.component';

describe('OrderCardDeletedComponent', () => {
  let component: OrderCardDeletedComponent;
  let fixture: ComponentFixture<OrderCardDeletedComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrderCardDeletedComponent]
    });
    fixture = TestBed.createComponent(OrderCardDeletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

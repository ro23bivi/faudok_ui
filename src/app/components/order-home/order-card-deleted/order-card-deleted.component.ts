import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PDFDocumentProxy } from 'ng2-pdf-viewer';
import { Observable } from 'rxjs';
import { TokenService } from 'src/app/services/token.service';
import { OrderResponseDto } from 'src/openapi/generated/models';

@Component({
  selector: 'app-order-card-deleted',
  templateUrl: './order-card-deleted.component.html',
  styleUrls: ['./order-card-deleted.component.css']
})
export class OrderCardDeletedComponent implements OnInit {

  /************************ deleted orders ****************************** */
    //admin actions
    @Output() view: EventEmitter<OrderResponseDto> = new EventEmitter<OrderResponseDto>();
    @Output() purge: EventEmitter<OrderResponseDto> = new EventEmitter<OrderResponseDto>();
    @Input() order: OrderResponseDto;
  
    //to display 1 page of a pdf document per time
    totalPagesPdf!: number;
    page: number = 1;
    isPdfLoaded: boolean = false;
    isPdfError: boolean = false;
    isNow$: Observable<boolean>;
    emailWorker = '';
  
    constructor(
      private tokenService: TokenService
    ) { }
  
    ngOnInit(): void {
      this.emailWorker = this.tokenService.getUsername();
    }

  
    onView() {
      this.view.emit(this.order);
    }
  
    onPurge() {
      this.purge.emit(this.order);
    }
  
  
    //to display 1 page of a pdf document per time
    callBackFn(pdf: PDFDocumentProxy){
      this.totalPagesPdf = pdf.numPages;
      this.isPdfLoaded = true;
      this.isPdfError = false;
    }
  
    onPdfError(error: any) {
      console.log(error);
      this.isPdfError = true;
    }
  
    nextPdfPage() {
      this.page++;
    }
  
    prevPdfPage() {
      this.page--;
    }
  
  }

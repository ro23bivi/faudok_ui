import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-order-reject-dialog',
  templateUrl: './order-reject-dialog.component.html',
  styleUrls: ['./order-reject-dialog.component.css']
})
export class OrderRejectDialogComponent {

  rejectForm: FormGroup;
  reasons: string[] = ['Document not found', 'Document not available', 'Legally not allowed', 'Other Reason'];

  constructor(
    public dialogRef: MatDialogRef<OrderRejectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder
  ) {
    this.rejectForm = this.fb.group({
      reason: [''],
      otherReason: [{ value: '', disabled: true }]
    });
    this.rejectForm.get('reason').valueChanges.subscribe(value => {
      if (value === 'Other Reason') {
        this.rejectForm.get('otherReason').enable();
      } else {
        this.rejectForm.get('otherReason').disable();
      }
    });
  }

    onNoClick(): void {
      this.dialogRef.close();
    }
  
    onSubmit(): void {
      if (this.rejectForm.valid) {
        const result = this.rejectForm.value;
        if (result.reason !== 'Other Reason') {
          result.otherReason = null;
        }
        this.dialogRef.close(result);
        }
    }

}

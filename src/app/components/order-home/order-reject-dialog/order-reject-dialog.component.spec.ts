import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderRejectDialogComponent } from './order-reject-dialog.component';

describe('OrderRejectDialogComponent', () => {
  let component: OrderRejectDialogComponent;
  let fixture: ComponentFixture<OrderRejectDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrderRejectDialogComponent]
    });
    fixture = TestBed.createComponent(OrderRejectDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

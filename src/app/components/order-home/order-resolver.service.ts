import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { TokenService } from 'src/app/services/token.service';
import { GetAnyOrderById$Params } from 'src/openapi/generated/fn/admin-order/get-any-order-by-id';
import { OrderResponseDto } from 'src/openapi/generated/models/order-response-dto';
import { AdminOrderService, ManagementService } from 'src/openapi/generated/services';
import { OrderService } from 'src/openapi/generated/services/order.service';

@Injectable({
  providedIn: 'root'
})
export class OrderDetailsResolverService implements Resolve<OrderResponseDto> {

  role = '';
  subscriptions: Subscription[] = [];

  constructor(
    private orderService: OrderService, 
    private adminOrderService: AdminOrderService, 
    private tokenService: TokenService,
    private managementService: ManagementService,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OrderResponseDto> | Promise<OrderResponseDto> | OrderResponseDto {
    this.initRole();
    this.subscriptions.forEach(sub => sub.unsubscribe());
    const orderId = route.params['orderId'];
    if(this.role == 'ROLE_USER') {
      return this.orderService.getChairsOrderById({orderId: orderId});
    } else if(this.role == 'ROLE_ADMIN') {
      return this.adminOrderService.getAnyOrderById({orderId: orderId});
    } else {
      throw new Error('No role found');
    }
  }

  initRole() {
    if(this.tokenService.isLoggedInSubject.getValue()){ 
      this.role =  this.tokenService.userRoles.length > 0 
          ? this.tokenService.userRoles[0] : '';
      if(this.role == '') {
        this.subscriptions.push(
          this.managementService.getRole().subscribe({
            next: (res) => {
              this.role = res;
            }
          })
        );
      }
    }
  }


}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChairsArchivedOrdersComponent } from './chairs-archived-orders.component';

describe('ChairsArchivedOrdersComponent', () => {
  let component: ChairsArchivedOrdersComponent;
  let fixture: ComponentFixture<ChairsArchivedOrdersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChairsArchivedOrdersComponent]
    });
    fixture = TestBed.createComponent(ChairsArchivedOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

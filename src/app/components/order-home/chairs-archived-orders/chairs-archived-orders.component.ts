import { AlertService } from 'src/app/services/alert.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GetChairsArchivedOrders$Params } from 'src/openapi/generated/fn/order/get-chairs-archived-orders';
import { FaudokResponsePageOrderResponseDto, OrderResponseDto } from 'src/openapi/generated/models';
import { OrderService } from 'src/openapi/generated/services/order.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertType } from 'src/app/alert/alert-type.enum';

@Component({
  selector: 'app-chairs-archived-orders',
  templateUrl: './chairs-archived-orders.component.html',
  styleUrls: ['./chairs-archived-orders.component.css']
})
export class ChairsArchivedOrdersComponent implements OnInit {

  
  ordersResponse: FaudokResponsePageOrderResponseDto = {};
  page = 0;
  size = 5;
  sortBy = 'doneDate';
  sortDir = 'desc';
  pages: any = [];

  constructor(
    private alertService: AlertService,
    private loadingService: LoadingService,
    private orderService: OrderService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.findChairsArchivedOrders();
  }

  private findChairsArchivedOrders() {
    this.loadingService.isLoading.next(true);
    let params: GetChairsArchivedOrders$Params = {
      pageNo: this.page,
      pageSize: this.size,
      sortBy: this.sortBy,
      sortDir: this.sortDir
    };
    this.orderService.getChairsArchivedOrders(params).subscribe({
      next: (res) => {
        this.loadingService.isLoading.next(false);
        this.ordersResponse = res;
        //get total nr of pages
        this.pages = Array(this.ordersResponse.totalPages)
            .fill(0)
            .map((x, i) => i);
      }, error: (err) => {
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert("Something went wrong. Please try again later.", AlertType.ERROR );
      }
    });
  }

  viewOrder(order: OrderResponseDto) {
    this.router.navigate(['orders','manage', order.id]);
  }

  //reassign to you, to manage reclamations
  reassignOrder(event) {
    //TODO
    //redirect to 'My Archived Orders'
  }
  
  gotToPage(page: number) {
    this.page = page;
    this.findChairsArchivedOrders();
  }

  goToFirstPage() {
    this.page = 0;
    this.findChairsArchivedOrders();
  }

  goToPreviousPage() {
    this.page --;
    this.findChairsArchivedOrders();
  }

  goToLastPage() {
    this.page = this.ordersResponse.totalPages as number - 1;
    this.findChairsArchivedOrders();
  }

  goToNextPage() {
    this.page++;
    this.findChairsArchivedOrders();
  }

  get isLastPage() {
    return this.page === this.ordersResponse.totalPages as number - 1;
  }
}


import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PDFDocumentProxy } from 'ng2-pdf-viewer';
import { Observable } from 'rxjs';
import { TokenService } from 'src/app/services/token.service';
import { OrderResponseDto } from 'src/openapi/generated/models';

@Component({
  selector: 'app-order-card-archived',
  templateUrl: './order-card-archived.component.html',
  styleUrls: ['./order-card-archived.component.css']
})
export class OrderCardArchivedComponent implements OnInit{


/*********************** archived orders  *********************************/
  //admin actions
  @Output() view: EventEmitter<OrderResponseDto> = new EventEmitter<OrderResponseDto>();
  @Output() reassign: EventEmitter<OrderResponseDto> = new EventEmitter<OrderResponseDto>();
  //user actions
  //view
  @Output() rescan: EventEmitter<OrderResponseDto> = new EventEmitter<OrderResponseDto>();
  @Input() order: OrderResponseDto;

  //to display 1 page of a pdf document per time
  totalPagesPdf!: number;
  page: number = 1;
  isPdfLoaded: boolean = false;
  isPdfError: boolean = false;
  isNow$: Observable<boolean>;
  emailWorker = '';

  constructor(
    private tokenService: TokenService
  ) { }

  ngOnInit(): void {
    this.emailWorker = this.tokenService.getUsername();
  }

  onView() {
    this.view.emit(this.order);
  }

  onReassign() {
    this.reassign.emit(this.order);
  }

  onRescan() {
    this.rescan.emit(this.order);
  }

  //to display 1 page of a pdf document per time
  callBackFn(pdf: PDFDocumentProxy){
    this.totalPagesPdf = pdf.numPages;
    this.isPdfLoaded = true;
    this.isPdfError = false;
  }

  onPdfError(error: any) {
    console.log(error);
    this.isPdfError = true;
  }

  nextPdfPage() {
    this.page++;
  }

  prevPdfPage() {
    this.page--;
  }

}

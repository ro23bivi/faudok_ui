import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderCardArchivedComponent } from './order-card-archived.component';

describe('OrderCardArchivedComponent', () => {
  let component: OrderCardArchivedComponent;
  let fixture: ComponentFixture<OrderCardArchivedComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrderCardArchivedComponent]
    });
    fixture = TestBed.createComponent(OrderCardArchivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { GetAllArchivedOrders$Params } from 'src/openapi/generated/fn/admin-order/get-all-archived-orders';
import { ReassignOrderFromOtherUser$Params } from 'src/openapi/generated/fn/admin-order/reassign-order-from-other-user';
import { FaudokResponsePageOrderResponseDto, OrderResponseDto } from 'src/openapi/generated/models';
import { AdminOrderService } from 'src/openapi/generated/services/admin-order.service';
import { OrderService } from 'src/openapi/generated/services/order.service';

@Component({
  selector: 'app-all-archived-orders',
  templateUrl: './all-archived-orders.component.html',
  styleUrls: ['./all-archived-orders.component.css']
})
export class AllArchivedOrdersComponent implements OnInit {

  
  ordersResponse: FaudokResponsePageOrderResponseDto = {};
  page = 0;
  size = 5;
  sortBy = 'doneDate';
  sortDir = 'desc';
  pages: any = [];

  constructor(
    private alertService: AlertService,
    private loadingService: LoadingService,
    private adminOrderService: AdminOrderService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.findAllArchivedOrders();
  }

  private findAllArchivedOrders() {
    this.loadingService.isLoading.next(true);
    let params: GetAllArchivedOrders$Params = {
      pageNo: this.page,
      pageSize: this.size,
      sortBy: this.sortBy,
      sortDir: this.sortDir
    };
    this.adminOrderService.getAllArchivedOrders(params).subscribe({
      next: (res) => {
        this.loadingService.isLoading.next(false);
        this.ordersResponse = res;
        //get total nr of pages
        this.pages = Array(this.ordersResponse.totalPages)
            .fill(0)
            .map((x, i) => i);
      }, error: (err) => {
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert("Something went wrong. Please try again later.", AlertType.ERROR );
      }
    });
  }

  viewOrder(order: OrderResponseDto) {
    this.router.navigate(['orders','manage', order.id]);
  }

  //reassign to you, to manage reclamations
  reassignOrder(order: OrderResponseDto) {
    this.loadingService.isLoading.next(true);
    const params: ReassignOrderFromOtherUser$Params = {
      orderId: order.id
    }
    this.adminOrderService.reassignOrderFromOtherUser(params).subscribe({
      next: (res) => {
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert('Order reassigned!', AlertType.SUCCESS );
        this.findAllArchivedOrders();
        //redirect to 'My Archived Orders'
        //this.router.navigate(['my-available-orders']);
      },
      error: (err) => {
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert("Something went wrong. Please try again later.", AlertType.ERROR );
      }
    })
  }
  

  
  gotToPage(page: number) {
    this.page = page;
    this.findAllArchivedOrders();
  }

  goToFirstPage() {
    this.page = 0;
    this.findAllArchivedOrders();
  }

  goToPreviousPage() {
    this.page --;
    this.findAllArchivedOrders();
  }

  goToLastPage() {
    this.page = this.ordersResponse.totalPages as number - 1;
    this.findAllArchivedOrders();
  }

  goToNextPage() {
    this.page++;
    this.findAllArchivedOrders();
  }

  get isLastPage() {
    return this.page === this.ordersResponse.totalPages as number - 1;
  }
}

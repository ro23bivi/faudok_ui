import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllArchivedOrdersComponent } from './all-archived-orders.component';

describe('AllArchivedOrdersComponent', () => {
  let component: AllArchivedOrdersComponent;
  let fixture: ComponentFixture<AllArchivedOrdersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AllArchivedOrdersComponent]
    });
    fixture = TestBed.createComponent(AllArchivedOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

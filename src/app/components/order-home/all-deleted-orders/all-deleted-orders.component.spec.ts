import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllDeletedOrdersComponent } from './all-deleted-orders.component';

describe('AllDeletedOrdersComponent', () => {
  let component: AllDeletedOrdersComponent;
  let fixture: ComponentFixture<AllDeletedOrdersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AllDeletedOrdersComponent]
    });
    fixture = TestBed.createComponent(AllDeletedOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

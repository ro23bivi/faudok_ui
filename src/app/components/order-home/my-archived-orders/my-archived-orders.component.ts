import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { GetChairsArchivedOrders$Params } from 'src/openapi/generated/fn/order/get-chairs-archived-orders';
import { GetUsersArchivedOrders$Params } from 'src/openapi/generated/fn/order/get-users-archived-orders';
import { FaudokResponsePageOrderResponseDto, OrderResponseDto } from 'src/openapi/generated/models';
import { OrderService } from 'src/openapi/generated/services';

@Component({
  selector: 'app-my-archived-orders',
  templateUrl: './my-archived-orders.component.html',
  styleUrls: ['./my-archived-orders.component.css']
})
export class MyArchivedOrdersComponent implements OnInit {

  
  ordersResponse: FaudokResponsePageOrderResponseDto = {};
  page = 0;
  size = 5;
  sortBy = 'doneDate';
  sortDir = 'desc';
  pages: any = [];

  constructor(
    private alertService: AlertService,
    private loadingService: LoadingService,
    private orderService: OrderService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.findMyArchivedOrders();
  }

  private findMyArchivedOrders() {
    this.loadingService.isLoading.next(true);
    let params: GetUsersArchivedOrders$Params = {
      pageNo: this.page,
      pageSize: this.size,
      sortBy: this.sortBy,
      sortDir: this.sortDir
    };
    this.orderService.getUsersArchivedOrders(params).subscribe({
      next: (res) => {
        this.loadingService.isLoading.next(false);
        this.ordersResponse = res;
        //get total nr of pages
        this.pages = Array(this.ordersResponse.totalPages)
            .fill(0)
            .map((x, i) => i);
      }, error: (err) => {
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert("Something went wrong. Please try again later.", AlertType.ERROR );
      }
    });
  }

  viewOrder(order: OrderResponseDto) {
    this.router.navigate(['orders','manage', order.id]);
  }

  //reassign to you, to manage reclamations
  rescanOrder(order: OrderResponseDto) {
    //TODO
    //show order details and QR code
    //on 'Done', send request to server
    //show 'Success'
    //redirect to 'my-archived-orders' to see also the rescanned order
    this.router.navigate(['orders','scan', order.id]);
  }
  
  gotToPage(page: number) {
    this.page = page;
    this.findMyArchivedOrders();
  }

  goToFirstPage() {
    this.page = 0;
    this.findMyArchivedOrders();
  }

  goToPreviousPage() {
    this.page --;
    this.findMyArchivedOrders();
  }

  goToLastPage() {
    this.page = this.ordersResponse.totalPages as number - 1;
    this.findMyArchivedOrders();
  }

  goToNextPage() {
    this.page++;
    this.findMyArchivedOrders();
  }

  get isLastPage() {
    return this.page === this.ordersResponse.totalPages as number - 1;
  }
}

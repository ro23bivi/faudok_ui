import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyArchivedOrdersComponent } from './my-archived-orders.component';

describe('MyArchivedOrdersComponent', () => {
  let component: MyArchivedOrdersComponent;
  let fixture: ComponentFixture<MyArchivedOrdersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyArchivedOrdersComponent]
    });
    fixture = TestBed.createComponent(MyArchivedOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

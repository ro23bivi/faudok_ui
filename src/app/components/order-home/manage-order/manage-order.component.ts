import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import cloneDeep from 'lodash/cloneDeep';
import { OrderResponseDto } from 'src/openapi/generated/models';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TokenService } from 'src/app/services/token.service';
import { AdminOrderService, ManagementService } from 'src/openapi/generated/services';
import { Subscription } from 'rxjs';
import { UpdateAnyOrdersMetadata$Params } from 'src/openapi/generated/fn/admin-order/update-any-orders-metadata';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertType } from 'src/app/alert/alert-type.enum';

@Component({
  selector: 'app-manage-order',
  templateUrl: './manage-order.component.html',
  styleUrls: ['./manage-order.component.css']
})
export class ManageOrderComponent implements OnInit, OnDestroy {

  editMode: boolean = false;
  orderForm: FormGroup;
  order: OrderResponseDto = null;
  role = '';
  //selectedPdf: File | undefined;
  subscriptions: Subscription[] = [];

  constructor(
    private loadingService: LoadingService,
    private alertService: AlertService,
    private adminOrderService: AdminOrderService,
    private tokenService: TokenService, 
    private managementService: ManagementService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.order = data['resolvedOrderDetails'];
      this.initRole();
      this.initForm();
    });
    //this.orderForm.patchValue(this.order);
  }

  initRole() {
    if(this.tokenService.isLoggedInSubject.getValue()){ 
      this.role =  this.tokenService.userRoles.length > 0 
          ? this.tokenService.userRoles[0] : '';
      if(this.role == '') {
        this.subscriptions.push(
          this.managementService.getRole().subscribe({
            next: (res) => {
              this.role = res;
            }
          })
        );
      }
    }
  }

  private initForm() {
    this.orderForm = this.fb.group({
      authorBook: new FormControl({value: this.order.authorBook, disabled: true}),
      authorArticle: new FormControl({value: this.order.authorArticle, disabled: true}),
      bvNr: new FormControl({value: this.order.bvNr, disabled: true },  [Validators.required]),
      clientDeliveryEmail: new FormControl({value: this.order.clientDeliveryEmail, disabled: true}),
      clientDeliveryName: new FormControl({value: this.order.clientDeliveryEmail, disabled: true}),
      isbn: new FormControl({value: this.order.isbn, disabled: true}),
      issn: new FormControl({value: this.order.issn, disabled: true}),
      issue: new FormControl({value: this.order.issue, disabled: true}),
      issueArticle: new FormControl({value: this.order.issueArticle, disabled: true}),
      pages: new FormControl({value: this.order.pages, disabled: true}, [Validators.required]),
      placePublication: new FormControl({value: this.order.placePublication, disabled: true}),
      publicationDate: new FormControl({value: this.order.publicationDate, disabled: true}),
      publisher: new FormControl({value: this.order.publisher, disabled: true}),
      signature: new FormControl({value: this.order.signature, disabled: true}, [Validators.required]),
      titleBook: new FormControl({value: this.order.titleBook, disabled: true}),
      titleArticle: new FormControl({value: this.order.titleArticle, disabled: true}),
      type: new FormControl({ value: this.order.type, disabled: true }, [Validators.required]), 
      //we allow only document related fields to be updated
      //id: [{ value: this.order.id, disabled: !this.editMode }], // Set as disabled if editing an existing order
      //transactionGroupQualifier: new FormControl({value: this.order.transactionGroupQualifier, disabled: true}),
      //scanPath: [{ value: this.order.publisher, disabled: !this.editMode }], 
      //userGroup: [{ value: this.order.userGroup, disabled: !this.editMode }, Validators.required],
      //state: [{ value: this.order.state, disabled: !this.editMode }, Validators.required],
      //doneDate: [{ value: this.order.dueDate, disabled: !this.editMode }],
      //dueDate: [{ value: this.order.dueDate, disabled: !this.editMode }, Validators.required],
    });
  }

  onEdit() {
    this.editMode = true;
    Object.keys(this.orderForm.controls).forEach(controlName => {
      // we dont allow edit for clientDeliveryEmail and clientDeliveryName
      if(controlName == 'clientDeliveryEmail' || controlName == 'clientDeliveryName') {
        this.orderForm.get(controlName).disable();
      } else {
        this.orderForm.get(controlName).enable();
      }
    });
  }

  onCancel(){ 
    this.editMode = false;
    this.orderForm.reset(this.order);
    Object.keys(this.orderForm.controls).forEach(controlName => {
      this.orderForm.get(controlName).disable();
    });
  }

  onSubmit() {
    this.loadingService.isLoading.next(true);
    //we do not modify original object, till we are sure that the form is valid and there is no error on server side
    let updatedOrder: OrderResponseDto = cloneDeep(this.order);
    Object.assign(updatedOrder, this.orderForm.value);
    const params: UpdateAnyOrdersMetadata$Params = {
      id: this.order.id,
      body: updatedOrder
    }
    if(this.orderForm.valid) {
      this.adminOrderService.updateAnyOrdersMetadata(params).subscribe(order =>{
        this.loadingService.isLoading.next(false);
        this.editMode = false;
        this.order = order;
        Object.keys(this.orderForm.controls).forEach(controlName => {
          this.orderForm.get(controlName).disable();
        });
        this.router.navigate(['/orders']);
      }, err => {
        this.alertService.showAlert('Failed to save the order. Please try again later.', AlertType.ERROR );
        this.loadingService.isLoading.next(false);
        this.onCancel();
        console.log(err);
      })
    }
  }


  goBack() {
    this.router.navigate(['/orders']);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

   //onFileUpload(file) {
    // TODO
   //}

}

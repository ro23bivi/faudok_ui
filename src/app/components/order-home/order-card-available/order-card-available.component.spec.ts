import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderCardAvailableComponent } from './order-card-available.component';

describe('OrderCardAvailableComponent', () => {
  let component: OrderCardAvailableComponent;
  let fixture: ComponentFixture<OrderCardAvailableComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrderCardAvailableComponent]
    });
    fixture = TestBed.createComponent(OrderCardAvailableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

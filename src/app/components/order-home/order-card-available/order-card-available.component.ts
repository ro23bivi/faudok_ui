import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { TokenService } from 'src/app/services/token.service';
import { OrderResponseDto } from 'src/openapi/generated/models';
import { ManagementService } from 'src/openapi/generated/services';

@Component({
  selector: 'app-order-card-available',
  templateUrl: './order-card-available.component.html',
  styleUrls: ['./order-card-available.component.css']
})
export class OrderCardAvailableComponent implements OnInit{

  
/********************** available orders ***********************/
  //admin actions
  //view
  //assign
  @Output() unassign: EventEmitter<OrderResponseDto> = new EventEmitter<OrderResponseDto>();  //unassign user when in vacation, to allow other usesr to work with the order
  @Output() move: EventEmitter<OrderResponseDto> = new EventEmitter<OrderResponseDto>();  //move to other chair if order is in wrong chair
  //user actions
  @Output() view: EventEmitter<OrderResponseDto> = new EventEmitter<OrderResponseDto>();
  @Output() assign: EventEmitter<OrderResponseDto> = new EventEmitter<OrderResponseDto>();
  @Output() scan: EventEmitter<OrderResponseDto> = new EventEmitter<OrderResponseDto>();
  @Output() reject: EventEmitter<OrderResponseDto> = new EventEmitter<OrderResponseDto>();
  @Input() order: OrderResponseDto;
  @Input() viewMode: boolean = false;

  //to display 1 page of a pdf document per time
  totalPagesPdf!: number;
  page: number = 1;
  isPdfLoaded: boolean = false;
  isPdfError: boolean = false;
  isNow$: Observable<boolean>;
  emailWorker = '';
  role= '';
  subscriptions: Subscription[] = [];

  constructor(
    private tokenService: TokenService,
    private managementService: ManagementService
  ) { }

  ngOnInit(): void {
    this.emailWorker = this.tokenService.getUsername();
    if(this.tokenService.isLoggedInSubject.getValue()){ 
      this.role =  this.tokenService.userRoles.length > 0 ? this.tokenService.userRoles[0] : '';
      if(this.role == '') {
        this.subscriptions.push(
          this.managementService.getRole().subscribe({
            next: (res) => {
              this.role = res;
            }
          })
        );
      }
    }
  }

  isToday(date: Date | string): boolean {
    return new Date().toDateString() === new Date(date).toDateString();
  }

  onView() {
    this.view.emit(this.order);
  }

  onAssign() {
    this.assign.emit(this.order);
  }

  onUnassign() {
    this.unassign.emit(this.order);
  }

  onMove() {
    this.move.emit(this.order);
  }

  onScan() {
    this.scan.emit(this.order);
  }

  onReject() {
    this.reject.emit(this.order);
  }

}

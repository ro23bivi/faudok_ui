import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { GetAllAvailableOrders$Params } from 'src/openapi/generated/fn/admin-order/get-all-available-orders';
import { AssignUnownedOrder$Params } from 'src/openapi/generated/fn/order/assign-unowned-order';
import { GetChairsAvailableOrders$Params } from 'src/openapi/generated/fn/order/get-chairs-available-orders';
import { FaudokResponsePageOrderResponseDto, OrderResponseDto } from 'src/openapi/generated/models';
import { OrderService, AdminOrderService } from 'src/openapi/generated/services';

@Component({
  selector: 'app-all-available-orders',
  templateUrl: './all-available-orders.component.html',
  styleUrls: ['./all-available-orders.component.css']
})
export class AllAvailableOrdersComponent implements OnInit{

  ordersResponse: FaudokResponsePageOrderResponseDto = {};
  page = 0;
  size = 5;
  sortBy = 'dueDate';
  sortDir = 'asc';
  pages: any = [];

  constructor(
    private loadingService: LoadingService,
    private alertService: AlertService,
    private adminOrderService: AdminOrderService,
    private orderService: OrderService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.findAllAvailableOrders();
  }

  private findAllAvailableOrders() {
    this.loadingService.isLoading.next(true);
    let params: GetAllAvailableOrders$Params = {
      pageNo: this.page,
      pageSize: this.size,
      sortBy: this.sortBy,
      sortDir: this.sortDir
    };
    this.adminOrderService.getAllAvailableOrders(params).subscribe({
      next: (res) => {
        this.loadingService.isLoading.next(false);
        this.ordersResponse = res;
        //get total nr of pages
        this.pages = Array(this.ordersResponse.totalPages)
            .fill(0)
            .map((x, i) => i);
      }, error: (err) => {
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert("Something went wrong. Please try again later.", AlertType.ERROR );
      }
    });
  }

  viewOrder(order: OrderResponseDto) {
    this.router.navigate(['orders','manage', order.id]);
  }

  
  gotToPage(page: number) {
    this.page = page;
    this.findAllAvailableOrders();
  }

  goToFirstPage() {
    this.page = 0;
    this.findAllAvailableOrders();
  }

  goToPreviousPage() {
    this.page --;
    this.findAllAvailableOrders();
  }

  goToLastPage() {
    this.page = this.ordersResponse.totalPages as number - 1;
    this.findAllAvailableOrders();
  }

  goToNextPage() {
    this.page++;
    this.findAllAvailableOrders();
  }

  get isLastPage() {
    return this.page === this.ordersResponse.totalPages as number - 1;
  }
  
}

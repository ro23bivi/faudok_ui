import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllAvailableOrdersComponent } from './all-available-orders.component';

describe('AllAvailableOrdersComponent', () => {
  let component: AllAvailableOrdersComponent;
  let fixture: ComponentFixture<AllAvailableOrdersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AllAvailableOrdersComponent]
    });
    fixture = TestBed.createComponent(AllAvailableOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

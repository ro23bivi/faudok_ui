import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderScanViewComponent } from './order-scan-view.component';

describe('OrderScanViewComponent', () => {
  let component: OrderScanViewComponent;
  let fixture: ComponentFixture<OrderScanViewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrderScanViewComponent]
    });
    fixture = TestBed.createComponent(OrderScanViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

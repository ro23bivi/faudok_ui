import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { jsPDF } from 'jspdf';
import { Subscription } from 'rxjs/internal/Subscription';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { ConfirmationDialogComponent } from 'src/app/confirmation-dialog/confirmation-dialog.component';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { TokenService } from 'src/app/services/token.service';
import { HandleScanOrder$Params } from 'src/openapi/generated/fn/order/handle-scan-order';
import { OrderResponseDto } from 'src/openapi/generated/models';
import { ManagementService, OrderService } from 'src/openapi/generated/services';

@Component({
  selector: 'app-order-scan-view',
  templateUrl: './order-scan-view.component.html',
  styleUrls: ['./order-scan-view.component.css']
})
export class OrderScanViewComponent implements OnInit, OnDestroy {

  @ViewChild('qrcode', { static: false }) qrcode: ElementRef;
  orderForm: FormGroup;
  order: OrderResponseDto = null;
  role = 'ROLE_USER';
  showOrderDetails: boolean = false; // Toggle to switch views
  //selectedPdf: File | undefined;
  subscriptions: Subscription[] = [];

  constructor(
    private loadingService: LoadingService,
    private alertService: AlertService,
    private orderService: OrderService,
    private tokenService: TokenService, 
    private managementService: ManagementService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.order = data['resolvedOrderDetails'];
      this.initRole()
      this.initForm();
    });
    //this.orderForm.patchValue(this.order);
  }

  initRole() {
    if(this.tokenService.isLoggedInSubject.getValue()){ 
      this.role =  this.tokenService.userRoles.length > 0 
          ? this.tokenService.userRoles[0] : '';
      if(this.role == '') {
        this.subscriptions.push(
          this.managementService.getRole().subscribe({
            next: (res) => {
              this.role = res;
            }
          })
        );
      }
    }
  }

  private initForm() {
    this.orderForm = this.fb.group({
      authorBook: new FormControl({value: this.order.authorBook, disabled: true}),
      authorArticle: new FormControl({value: this.order.authorArticle, disabled: true}),
      bvNr: new FormControl({value: this.order.bvNr, disabled: true },  [Validators.required]),
      clientDeliveryEmail: new FormControl({value: this.order.clientDeliveryEmail, disabled: true},  [Validators.required, Validators.email]),
      clientDeliveryName: new FormControl({value: this.order.clientDeliveryEmail, disabled: true}, [Validators.required]),
      isbn: new FormControl({value: this.order.isbn, disabled: true}),
      issn: new FormControl({value: this.order.issn, disabled: true}),
      issue: new FormControl({value: this.order.issue, disabled: true}),
      issueArticle: new FormControl({value: this.order.issueArticle, disabled: true}),
      pages: new FormControl({value: this.order.pages, disabled: true}, [Validators.required]),
      placePublication: new FormControl({value: this.order.placePublication, disabled: true}),
      publicationDate: new FormControl({value: this.order.publicationDate, disabled: true}),
      publisher: new FormControl({value: this.order.publisher, disabled: true}),
      signature: new FormControl({value: this.order.signature, disabled: true}, [Validators.required]),
      titleBook: new FormControl({value: this.order.titleBook, disabled: true}),
      titleArticle: new FormControl({value: this.order.titleArticle, disabled: true}),
      type: new FormControl({ value: this.order.type, disabled: true }, [Validators.required]), 
      //we allow only document related fields to be updated
      //id: [{ value: this.order.id, disabled: !this.editMode }], // Set as disabled if editing an existing order
      //scanPath: [{ value: this.order.publisher, disabled: !this.editMode }], 
      //transactionGroupQualifier: new FormControl({value: this.order.transactionGroupQualifier, disabled: true}),
      //userGroup: [{ value: this.order.userGroup, disabled: !this.editMode }, Validators.required],
      //state: [{ value: this.order.state, disabled: !this.editMode }, Validators.required],
      //doneDate: [{ value: this.order.dueDate, disabled: !this.editMode }],
      //dueDate: [{ value: this.order.dueDate, disabled: !this.editMode }, Validators.required],
    });
  }


  onDone() {
    //TODO show all infos about book and the QR code
    //and a 'Done' button -> when 'Done' is pressed, and confirmation is given, send the request
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);   
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loadingService.isLoading.next(true);
        const params: HandleScanOrder$Params = {
          orderId: this.order.id
        }
        if(this.order.scanPath && this.order.scanned) {
          this.orderService.handleRescanOrder(params).subscribe({
            next: (res) => {
              this.loadingService.isLoading.next(false);
              this.alertService.showAlert('Order rescanned!', AlertType.SUCCESS );
              this.router.navigate(['/orders']);
            },
            error: (err) => {
              this.alertService.showAlert('Rescan Failed. Please try again later.', AlertType.ERROR );
              this.loadingService.isLoading.next(false);
            }
          });
        } else {
          this.orderService.handleScanOrder(params).subscribe({
            next: (res) => {
              this.loadingService.isLoading.next(false);
              this.alertService.showAlert('Order scanned!', AlertType.SUCCESS );
              this.router.navigate(['/orders']);
            },
            error: (err) => {
              this.alertService.showAlert('Scan Failed. Please try again later.', AlertType.ERROR );
              this.loadingService.isLoading.next(false);
            }
          });
        }
     }
   });  
  }

  downloadQrCode() {
    let yposition= 85;
    const doc = new jsPDF();
    const qrCodeElement = this.qrcode.nativeElement.querySelector('canvas');
    const imgData = qrCodeElement.toDataURL('image/png');
    doc.addImage(imgData, 'PNG', 60, 5, 80, 80);
    // Add order details to PDF
    doc.setFontSize(12);
    if(this.order.titleBook) { yposition = yposition + 10; doc.text(`Title Book: ${this.order.titleBook}`, 10, yposition); }
    if(this.order.authorBook) { yposition = yposition + 10; doc.text(`Author Book: ${this.order.authorBook}`, 10, yposition); }
    if(this.order.titleArticle) { yposition = yposition + 10; doc.text(`Title Article: ${this.order.titleArticle}`, 10, yposition); }
    if(this.order.authorArticle) { yposition = yposition + 10; doc.text(`Author Article: ${this.order.authorArticle}`, 10, yposition); }
    if(this.order.isbn) { yposition = yposition + 10; doc.text(`ISBN: ${this.order.isbn}`, 10, yposition); }
    if(this.order.issn) { yposition = yposition + 10; doc.text(`ISSN: ${this.order.issn}`, 10, yposition); }
    if(this.order.issue) { yposition = yposition + 10; doc.text(`Issue: ${this.order.issue}`, 10, yposition); }
    if(this.order.issueArticle) { yposition = yposition + 10; doc.text(`Issue Article: ${this.order.issueArticle}`, 10, yposition); }
    yposition = yposition + 10;
    doc.text(`BV Nr: ${this.order.bvNr}`, 10, yposition);
    yposition = yposition + 10;
    doc.text(`Signature: ${this.order.signature}`, 10, yposition);
    yposition = yposition + 10;
    doc.text(`Pages: ${this.order.pages}`, 10, yposition);
    yposition = yposition + 10;
    doc.text(`Place Publication: ${this.order.placePublication}`, 10, yposition);
    yposition = yposition + 10;
    doc.text(`Date Publication: ${this.order.publicationDate}`, 10, yposition);
    yposition = yposition + 10;
    doc.text(`Publisher: ${this.order.publisher}`, 10, yposition);
    yposition = yposition + 10;
    doc.text(`Type: ${this.order.type}`, 10, yposition);
    yposition = yposition + 10;
    doc.text(`Client Delivery Email: ${this.order.clientDeliveryEmail}`, 10, yposition);
    yposition = yposition + 10;
    doc.text(`Client Delivery Name: ${this.order.clientDeliveryName}`, 10, yposition );
    yposition = yposition + 10;
    doc.text(`Due Date: ${this.order.dueDate}`, 10, yposition);
    //doc.text(`Worker ID: ${this.order.faudokWorkerId || 'Unassigned'}`, 10, 80);

    // Save PDF
    doc.save('QRCode_Order_Details.pdf');
  }

  goBack() {
    this.router.navigate(['/orders']);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }


}

import { OrderService } from './../../../openapi/generated/services/order.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { TokenService } from 'src/app/services/token.service';
import { ManagementService } from 'src/openapi/generated/services/management.service';

@Component({
  selector: 'app-order-home',
  templateUrl: './order-home.component.html',
  styleUrls: ['./order-home.component.css']
})
export class OrderHomeComponent implements OnInit, OnDestroy {
  
  private subscriptions: Subscription[] = [];
  role = '';
  emailWorker = '';
  selectedTabIndex: number = 0;


  constructor(
    private loadingService: LoadingService,
    private alertService: AlertService,
    private managementService: ManagementService,
    private tokenService: TokenService,
  ) {
  }

  ngOnInit() {
    this.emailWorker = this.tokenService.getUsername();
    console.log('****Username: ' + this.emailWorker);
    if(this.tokenService.isLoggedInSubject.getValue()){ 
      this.role =  this.tokenService.userRoles.length > 0 ? this.tokenService.userRoles[0] : '';
      if(this.role == '') {
        this.subscriptions.push(
          this.managementService.getRole().subscribe({
            next: (res) => {
              this.role = res;
            }
          })
        );
      }
    }
  }


  onTabChange(event: any): void {
    this.selectedTabIndex = event.index;
  }

  // onProfilePictureSelected(event: any): void {
  //   console.log(event);
  //   this.profilePicture = event.target.files[0] as File;
  //   console.log(this.profilePicture);
  //   this.profilePictureChange = true;
  // }

  
  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }  
}

import { AdminOrderService, ManagementService, OrderService } from 'src/openapi/generated/services';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FaudokResponsePageOrderResponseDto } from 'src/openapi/generated/models';
import { Router } from '@angular/router';
import { GetUsersAvailableOrders$Params } from 'src/openapi/generated/fn/order/get-users-available-orders';
import { OrderResponseDto } from 'src/openapi/generated/models/order-response-dto';
import { HandleRejectOrder$Params } from 'src/openapi/generated/fn/order/handle-reject-order';
import { UnassignOwnedOrder$Params } from 'src/openapi/generated/fn/order/unassign-owned-order';
import { TokenService } from 'src/app/services/token.service';
import { Subscription } from 'rxjs';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { MatDialog } from '@angular/material/dialog';
import { OrderRejectDialogComponent } from '../order-reject-dialog/order-reject-dialog.component';

@Component({
  selector: 'app-my-available-orders',
  templateUrl: './my-available-orders.component.html',
  styleUrls: ['./my-available-orders.component.css']
})
export class MyAvailableOrdersComponent implements OnInit, OnDestroy{

  ordersResponse: FaudokResponsePageOrderResponseDto = {};
  page = 0;
  size = 5;
  sortBy = 'dueDate';
  sortDir = 'asc';
  pages: any = [];
  role = '';
  subscriptions: Subscription[] = [];

  constructor(
    private loadingService: LoadingService,
    private alertService: AlertService,
    private orderService: OrderService,
    private router: Router,
    private tokenService: TokenService,
    private managementService: ManagementService,
    private dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    if(this.tokenService.isLoggedInSubject.getValue()){ 
      this.role =  this.tokenService.userRoles.length > 0 
          ? this.tokenService.userRoles[0] : '';
      if(this.role == '') {
        this.subscriptions.push(
          this.managementService.getRole().subscribe({
            next: (res) => {
              this.role = res;
            }
          })
        );
      }
    }
    this.findMyAvailableOrders();
  }

  private findMyAvailableOrders() {
    let params: GetUsersAvailableOrders$Params = {
      pageNo: this.page,
      pageSize: this.size,
      sortBy: this.sortBy,
      sortDir: this.sortDir
    };
    this.loadingService.isLoading.next(true);
    this.orderService.getUsersAvailableOrders(params).subscribe({
      next: (res) => {
        this.loadingService.isLoading.next(false);
        this.ordersResponse = res;
        //console.log(this.ordersResponse)
        //get total nr of pages
        this.pages = Array(this.ordersResponse.totalPages)
            .fill(0)
            .map((x, i) => i);
      }, error: (err) => {
        //console.log(err);
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert('Something went wrong. Please try again later.', AlertType.ERROR );
      }
    });
  }


  viewOrder(order: OrderResponseDto) {
    this.router.navigate(['orders','manage', order.id]);
  }

  unassignOwnedOrder(order: OrderResponseDto) {
    this.loadingService.isLoading.next(true);
    const params: UnassignOwnedOrder$Params = {
      orderId: order.id
    };
    this.orderService.unassignOwnedOrder(params).subscribe({
      next: (res) => {
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert('Order unassigned successfully', AlertType.SUCCESS );
        this.findMyAvailableOrders();
      },
      error: (err) => {
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert('Something went wrong. Please try again later.', AlertType.ERROR );
      }
    })
  }


  scanOrder(order: OrderResponseDto) {
    this.router.navigate(['orders','scan', order.id]);
  }


  rejectOrder(order: OrderResponseDto) {
    let reasonRejectionModal = '';
    //open modal and let worker select reason of rejection
    const dialogRef = this.dialog.open(OrderRejectDialogComponent, { width: '300px', data: {} });
    dialogRef.afterClosed().subscribe(result => {
      if (result.reason) {
        // Handle the rejection reason
        console.log('Rejection reason:', result);
        if (result.reason === 'Other Reason') {
          reasonRejectionModal = result.otherReason;
        } else {
          reasonRejectionModal = result.reason;
        }
        this.loadingService.isLoading.next(true);
        const param: HandleRejectOrder$Params = {
          orderId: order.id,
          body: { reasonRejection: reasonRejectionModal }
        }
        this.orderService.handleRejectOrder(param).subscribe({
          next: (res) => {
            this.loadingService.isLoading.next(false);
            this.alertService.showAlert('Order rejected successfully', AlertType.SUCCESS );
            this.findMyAvailableOrders();
          },
          error: (err) => {
            this.loadingService.isLoading.next(false);
            this.alertService.showAlert('Something went wrong. Please try again later.', AlertType.ERROR );
          }
        })
      }
    });
 }

      gotToPage(page: number) {
        this.page = page;
        this.findMyAvailableOrders();
      }
    
      goToFirstPage() {
        this.page = 0;
        this.findMyAvailableOrders();
      }
    
      goToPreviousPage() {
        this.page --;
        this.findMyAvailableOrders();
      }
    
      goToLastPage() {
        this.page = this.ordersResponse.totalPages as number - 1;
        this.findMyAvailableOrders();
      }
    
      goToNextPage() {
        this.page++;
        this.findMyAvailableOrders();
      }
    
      get isLastPage() {
        return this.page === this.ordersResponse.totalPages as number - 1;
      }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
  
}

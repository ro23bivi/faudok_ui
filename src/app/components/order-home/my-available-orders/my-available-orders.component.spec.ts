import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAvailableOrdersComponent } from './my-available-orders.component';

describe('MyAvailableOrdersComponent', () => {
  let component: MyAvailableOrdersComponent;
  let fixture: ComponentFixture<MyAvailableOrdersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyAvailableOrdersComponent]
    });
    fixture = TestBed.createComponent(MyAvailableOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChairsAvailableOrdersComponent } from './chairs-available-orders.component';

describe('ChairsAvailableOrdersComponent', () => {
  let component: ChairsAvailableOrdersComponent;
  let fixture: ComponentFixture<ChairsAvailableOrdersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChairsAvailableOrdersComponent]
    });
    fixture = TestBed.createComponent(ChairsAvailableOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

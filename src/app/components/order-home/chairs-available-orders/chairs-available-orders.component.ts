import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AlertType } from 'src/app/alert/alert-type.enum';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { UnassignAnyOrder$Params } from 'src/openapi/generated/fn/admin-order/unassign-any-order';
import { AssignUnownedOrder$Params } from 'src/openapi/generated/fn/order/assign-unowned-order';
import { GetChairsAvailableOrders$Params } from 'src/openapi/generated/fn/order/get-chairs-available-orders';
import { FaudokResponsePageOrderResponseDto, OrderResponseDto } from 'src/openapi/generated/models';
import { AdminOrderService, OrderService } from 'src/openapi/generated/services';
import { MoveOrderDialogComponent } from '../move-order-dialog/move-order-dialog.component';
import { MoveOrder$Params } from 'src/openapi/generated/fn/admin-order/move-order';

@Component({
  selector: 'app-chairs-available-orders',
  templateUrl: './chairs-available-orders.component.html',
  styleUrls: ['./chairs-available-orders.component.css']
})
export class ChairsAvailableOrdersComponent implements OnInit{


  ordersResponse: FaudokResponsePageOrderResponseDto = {};
  page = 0;
  size = 5;
  sortBy = 'dueDate';
  sortDir = 'asc';
  pages: any = [];

  constructor(
    private loadingService: LoadingService,
    private alertService: AlertService,
    private orderService: OrderService,
    private adminOrderService: AdminOrderService,
    private router: Router,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.findChairsAvailableOrders();
  }

  private findChairsAvailableOrders() {
    this.loadingService.isLoading.next(true);
    let params: GetChairsAvailableOrders$Params = {
      pageNo: this.page,
      pageSize: this.size,
      sortBy: this.sortBy,
      sortDir: this.sortDir
    };
    this.orderService.getChairsAvailableOrders(params).subscribe({
      next: (res) => {
        this.loadingService.isLoading.next(false);
        this.ordersResponse = res;
        //get total nr of pages
        this.pages = Array(this.ordersResponse.totalPages)
            .fill(0)
            .map((x, i) => i);
      }, error: (err) => {
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert("Something went wrong. Please try again later.", AlertType.ERROR );
      }
    });
  }


  viewOrder(order: OrderResponseDto) {
    this.router.navigate(['orders','manage', order.id]);
  }

  assignUnownedOrder(order: OrderResponseDto) {
    this.loadingService.isLoading.next(true);
    const params: AssignUnownedOrder$Params = {
      orderId: order.id
    };
    this.orderService.assignUnownedOrder(params).subscribe({
      next: (res) => {
        this.loadingService.isLoading.next(false);
        this.findChairsAvailableOrders();
        this.alertService.showAlert("Order assigned succesfully", AlertType.SUCCESS );
      },
      error: (err) => {
        this.loadingService.isLoading.next(false);
        this.alertService.showAlert("Something went wrong. Please try again later.", AlertType.ERROR );
      }
    });
  }

    moveOrderToOtherChair(order: OrderResponseDto) {
      const dialogRef = this.dialog.open(MoveOrderDialogComponent, { width: '300px', data: {} });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.loadingService.isLoading.next(true);
          this.adminOrderService.moveOrder( {id: order.id, chair: result.selectedChair} ).subscribe({
            next: (res) => {
              this.loadingService.isLoading.next(false);
              this.findChairsAvailableOrders();
              this.alertService.showAlert("Order moved succesfully", AlertType.SUCCESS );
            },
            error: (err) => {
              this.loadingService.isLoading.next(false);
              this.alertService.showAlert("Something went wrong. Please try again later.", AlertType.ERROR );
            }
          })
        }
      });
    }

    unassignOrder(order: OrderResponseDto) {
       this.loadingService.isLoading.next(true);
      this.adminOrderService.unassignAnyOrder( {id: order.id} ).subscribe({
        next: (res) => {
          this.loadingService.isLoading.next(false);
          this.alertService.showAlert("Order unassigned successfully", AlertType.SUCCESS );
          this.findChairsAvailableOrders();
          //this.router.navigate(['orders', 'my-available-orders']);
        },
        error: (err) => {
          console.log(err);
          this.loadingService.isLoading.next(false);
          this.alertService.showAlert("Something went wrong. Please try again later.", AlertType.ERROR );
        }
      })
    }

    gotToPage(page: number) {
      this.page = page;
      this.findChairsAvailableOrders();
    }

    goToFirstPage() {
      this.page = 0;
      this.findChairsAvailableOrders();
    }

    goToPreviousPage() {
      this.page --;
      this.findChairsAvailableOrders();
    }

    goToLastPage() {
      this.page = this.ordersResponse.totalPages as number - 1;
      this.findChairsAvailableOrders();
    }

    goToNextPage() {
      this.page++;
      this.findChairsAvailableOrders();
    }

    get isLastPage() {
      return this.page === this.ordersResponse.totalPages as number - 1;
    }
  
}

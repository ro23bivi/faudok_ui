import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './components/contact/contact.component';
import { LoginComponent } from './components/login/login.component';
import { AuthActivateRouteGuard } from './routeguards/auth.routeguard';
import { HomeComponent } from './components/home/home.component';
//import { RegisterComponent } from './components/register/register.component';
import { FaqComponent } from './components/faq/faq.component';
import { OrderHomeComponent } from './components/order-home/order-home.component';
import { ActivateComponent } from './components/activate-account/activate.component';
import { AdminActivateRouteGuard } from './routeguards/admin.routeguard ';
import { ManageOrderComponent } from './components/order-home/manage-order/manage-order.component';
import { OrderDetailsResolverService } from './components/order-home/order-resolver.service';
import { ChairsAvailableOrdersComponent } from './components/order-home/chairs-available-orders/chairs-available-orders.component';
import { AllArchivedOrdersComponent } from './components/order-home/all-archived-orders/all-archived-orders.component';
import { AllAvailableOrdersComponent } from './components/order-home/all-available-orders/all-available-orders.component';
import { AllDeletedOrdersComponent } from './components/order-home/all-deleted-orders/all-deleted-orders.component';
import { ChairsArchivedOrdersComponent } from './components/order-home/chairs-archived-orders/chairs-archived-orders.component';
import { ChairsDeletedOrdersComponent } from './components/order-home/chairs-deleted-orders/chairs-deleted-orders.component';
import { MyArchivedOrdersComponent } from './components/order-home/my-archived-orders/my-archived-orders.component';
import { MyAvailableOrdersComponent } from './components/order-home/my-available-orders/my-available-orders.component';
import { UserInfoComponent } from './components/user/user-info/user-info.component';
import { UserRegistrationComponent } from './components/user/user-registration/user-registration.component';
import { OrderScanViewComponent } from './components/order-home/order-scan-view/order-scan-view.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ForgotPasswordValidationComponent } from './components/forgot-password-validation/forgot-password-validation.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'preset', component: ForgotPasswordValidationComponent },
  { path: 'activate-account', component: ActivateComponent},
  // { path: 'contact', component: ContactComponent},
  { path: 'faq', component: FaqComponent},
  // { path: 'register', component: RegisterComponent, canActivate: [AuthActivateRouteGuard], data: { roles: ['ROLE_ADMIN'] }, }, 
  { path: 'orders', component: OrderHomeComponent, canActivate: [AuthActivateRouteGuard] },
  { path: 'orders/my-available-orders', component: MyAvailableOrdersComponent, canActivate: [AuthActivateRouteGuard] },
  { path: 'orders/chairs-available-orders', component: ChairsAvailableOrdersComponent, canActivate: [AuthActivateRouteGuard] },
  { path: 'orders/all-available-orders', component: AllAvailableOrdersComponent, canActivate: [AuthActivateRouteGuard] },
  { path: 'orders/my-archived-orders', component: MyArchivedOrdersComponent, canActivate: [AuthActivateRouteGuard] },
  { path: 'orders/chairs-archived-orders', component: ChairsArchivedOrdersComponent, canActivate: [AuthActivateRouteGuard] },
  { path: 'orders/all-archived-orders', component: AllArchivedOrdersComponent, canActivate: [AuthActivateRouteGuard] },
  { path: 'orders/chairs-deleted-orders', component: ChairsDeletedOrdersComponent, canActivate: [AuthActivateRouteGuard] },
  { path: 'orders/all-deleted-orders', component: AllDeletedOrdersComponent, canActivate: [AuthActivateRouteGuard] },
  { path: 'orders/manage/:orderId', component: ManageOrderComponent, canActivate: [AuthActivateRouteGuard], resolve: {resolvedOrderDetails: OrderDetailsResolverService} },
  { path: 'orders/scan/:orderId', component: OrderScanViewComponent, canActivate: [AuthActivateRouteGuard], resolve: {resolvedOrderDetails: OrderDetailsResolverService} },
  { path: 'account', component: UserInfoComponent, canActivate: [AuthActivateRouteGuard] },
  { path: 'register', component: UserRegistrationComponent, canActivate: [AdminActivateRouteGuard] },
  {path: '**', redirectTo: 'home'}
  //{path: '**', component: NotfoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


export const  AppConstants = {
    CLIENT_URL: "http://localhost:4200",
    CONTACT_API_URL: "/contact",
    LOGIN_API_URL: "/signin",
    //REGISTER_API_URL : "/register",
    ACCOUNT_API_URL: "/myAccount",
    LOGOUT_API_URL: "/signout",
    HOME_API_URL: "/home",
    ORDERS_API_URL: "/orders",
    FAQ_API_URL: "/faq",
    
    SERVER_URL: "http://localhost:8080/api/v1",
    SERVER_ORDERS_URL: "/orders",
    
}

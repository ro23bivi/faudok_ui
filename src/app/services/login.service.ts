// import { Injectable } from '@angular/core';
// import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
// import { Observable, Subject } from 'rxjs';
// import { AppConstants } from 'src/app/constants/app.constants';
// import { environment } from 'src/environments/environment';

// @Injectable({
//   providedIn: 'root'
// })
// export class LoginService {

//   constructor(private http: HttpClient) {  
//   }

//   validateLoginDetails(user: User) {
//     window.sessionStorage.setItem("userdetails", JSON.stringify(user));
//     //withCredentials=true > tell Angular to get frm browser localhost:4200 the cookie JSESSIONID and send it to REST API with the request
//     return this.http.get(environment.rooturl + AppConstants.LOGIN_API_URL, { observe: 'response', withCredentials: true });
//   }
// }

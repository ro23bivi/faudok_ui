import { Injectable, OnInit } from '@angular/core';
// import { JwtHelperService } from '@auth0/angular-jwt/lib/jwthelper.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenService  implements OnInit{

  logedInUsername: string | null;
  redirectUrl: string;
  isLoggedInSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  
  constructor() { 
    this.isLoggedInTrace();
  }

  ngOnInit(): void {
  }

  isLoggedInTrace() {
    const jwtHelper = new JwtHelperService();
    if (this.token != null && this.token !== '') {
      if ((jwtHelper.decodeToken(this.token).sub != null) || (jwtHelper.decodeToken(this.token).sub !== '')) {
        if (!jwtHelper.isTokenExpired(this.token)) {
          //user logged in, so update state
          this.isLoggedInSubject.next(true);
          this.logedInUsername = jwtHelper.decodeToken(this.token).sub;
        }
      }
    } 
    //otherwise is not logged in, remove token
    this.removeToken();
    this.isLoggedInSubject.next(false);
  }

  set token(token: string) {
    localStorage.setItem('token', token);
  }

  get token() {
    return localStorage.getItem('token') as string;
  }

  removeToken() {
    localStorage.removeItem('token');
  }

  isTokenValid() {
    const token = this.token;
    if (!token) {
      return false;
    }
    // decode the token
    const jwtHelper = new JwtHelperService();
    // check expiry date
    const isTokenExpired = jwtHelper.isTokenExpired(token);
    if (isTokenExpired) {
      localStorage.clear();
      return false;
    }
    return true;
  }

  isTokenNotValid() {
    return !this.isTokenValid();
  }

  get userRoles(): string[] {
    const token = this.token;
    if (token) {
      const jwtHelper = new JwtHelperService();
      const decodedToken = jwtHelper.decodeToken(token);
      console.log(decodedToken.authorities);
      return decodedToken.authorities;
    }
    return [];
  }

  getUsername(): string {
    const token = this.token;
    if (token) {
      const jwtHelper = new JwtHelperService();
      const decodedToken = jwtHelper.decodeToken(token);
      console.log(decodedToken.authorities);
      return decodedToken.sub;
    }
    return '';
  }


    isLoggedIn(): boolean {
    const jwtHelper = new JwtHelperService();
    if (this.token != null && this.token !== '') {
      if ((jwtHelper.decodeToken(this.token).sub != null) || (jwtHelper.decodeToken(this.token).sub !== '')) {
        if (!jwtHelper.isTokenExpired(this.token)) {
          this.logedInUsername = jwtHelper.decodeToken(this.token).sub;
          return true;
        }
      }
    } 
    //otherwise is not logged in, remove token
    this.removeToken();
    return false;
  }

}

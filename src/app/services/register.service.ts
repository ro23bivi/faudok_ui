// import { Injectable } from '@angular/core';
// import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
// import { AppConstants } from 'src/app/constants/app.constants';
// import { environment } from '../../environments/environment';
// import { RegisteredUser } from 'src/app/model/registered-user.model';

// @Injectable({
//   providedIn: 'root'
// })
// export class RegisterService {

//   constructor(private http: HttpClient) {  
//   }

//   validateRegisterDetails(user: RegisteredUser) {
//     //withCredentials=true > tell Angular to get frm browser localhost:4200 the cookie JSESSIONID and send it to REST API with the request
//     return this.http.post(environment.rooturl + AppConstants.REGISTER_API_URL, user, { observe: 'response', withCredentials: true });
//   }

// }

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { Alert } from '../alert/alert';
import { AlertType } from '../alert/alert-type.enum';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private snackBar: MatSnackBar) { }

  public alerts: Subject<Alert> = new Subject();

  showAlert(message: string, alterType: AlertType, duration: number = 3000): void {
    const alert = new Alert(message, alterType, duration);
    this.alerts.next(alert);
  }
  
}

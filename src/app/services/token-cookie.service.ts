// import { Injectable } from '@angular/core';
// import { JwtHelperService } from '@auth0/angular-jwt';

// @Injectable({
//   providedIn: 'root'
// })
// export class TokenCookieService {

//   logedInUsername: string | null;
//   redirectUrl: string;
//   private readonly TOKEN_COOKIE_NAME = 'token';
//   private readonly DOMAIN = '.faudok.de';


//   //to upload PDF?
//   // uploadeUserProfilePicture(profilePicture: File) {
//   //   const fd = new FormData();
//   //   fd.append('image', profilePicture);
//   //   return this.http
//   //     .post(`${this.host}/user/photo/upload`, fd, { responseType: 'text'})
//   //     .subscribe(
//   //       (response: any) => {
//   //         console.log(response);
//   //         console.log('User profile picture was uploaded. ' + response);
//   //       },
//   //       error => {
//   //         console.log(error);
//   //       }
//   //     );
//   //   }


//   isLoggedIn(): boolean {
//     const jwtHelper = new JwtHelperService();
//     if (this.getToken() != null && this.getToken() !== '') {
//       if ((jwtHelper.decodeToken(this.getToken()).sub != null) || (jwtHelper.decodeToken(this.getToken()).sub !== '')) {
//         if (!jwtHelper.isTokenExpired(this.getToken())) {
//           this.logedInUsername = jwtHelper.decodeToken(this.getToken()).sub;
//           return true;
//         }
//       }
//     } 
//     //otherwise is not logged in, remove token
//     this.removeToken();
//     return false;
//   }


//   // Store the token in a cookie
//   storeToken(token: string): void {
//     const cookieOptions = `secure; HttpOnly; domain=${this.DOMAIN}; expires=${new Date(Date.now() + 7 * 24 * 60 * 60 * 1000).toUTCString()}`;
//     document.cookie = `${this.TOKEN_COOKIE_NAME}=${token}; ${cookieOptions}`;
//   }

//   // Get the token from the cookie
//   getToken(): string | null {
//     const cookies = document.cookie.split('; ');
//     for (const cookie of cookies) {
//       const [name, value] = cookie.split('=');
//       if (name === this.TOKEN_COOKIE_NAME) {
//         return value;
//       }
//     }
//     return null;
//   }

//   // Remove the token from the cookie
//   removeToken(): void {
//     document.cookie = `${this.TOKEN_COOKIE_NAME}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/; domain=${this.DOMAIN}`;
//   }

//   isTokenValid() {
//     const token = this.getToken();
//     if (!token) {
//       return false;
//     }
//     // decode the token
//     const jwtHelper = new JwtHelperService();
//     // check expiry date
//     const isTokenExpired = jwtHelper.isTokenExpired(token);
//     if (isTokenExpired) {
//       localStorage.clear();
//       return false;
//     }
//     return true;
//   }

//   isTokenNotValid() {
//     return !this.isTokenValid();
//   }

//   getUserRoles(): string[] {
//     const token = this.getToken();
//     if (token) {
//       const jwtHelper = new JwtHelperService();
//       const decodedToken = jwtHelper.decodeToken(token);
//       console.log(decodedToken.authorities);
//       return decodedToken.authorities;
//     }
//     return [];
//   }

// }

// import { Injectable } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
// import { AppConstants } from "../constants/app.constants";
// import { environment } from '../../environments/environment';
// import { Contact } from '../model/contact.model';

// @Injectable({
//   providedIn: 'root'
// })
// export class DashboardService {

//   constructor(private http:HttpClient) { }

//   getAccountDetails(id: number){  
//     //java generates autom 2 cookies: JSEESSIONID and X-XSRG-TOKEN  
//     //cookie JSEESSIONID: once logged in, user is logged in for all other subsequent request till logged out
//     //cookie XSRF-TOKEN: for CRSF protection,for PUT/POST/DELETE need to send XSRF token in header of request from Angular,s.t. Java can recognize that the request was made from our trusted frontend
//     //withCredentials=true > tell Angular to get frm browser localhost:4200 the cookies JSESSIONID and send it to REST API with the request
//     // observe: response to get all response details aka body, headers, cookies etc
//     return this.http.get(environment.rooturl + AppConstants.ACCOUNT_API_URL + "?id="+id, { observe: 'response', withCredentials: true });
//   }  

//   getAccountTransactions(id: number){
//     //withCredentials=true > tell Angular to get frm browser localhost:4200 the cookie JSESSIONID and send it to REST API with the request
//     //return this.http.get(environment.rooturl + AppConstants.BALANCE_API_URL+ "?id="+id,{ observe: 'response', withCredentials: true });
//   }

//   saveMessage(contactMessage: Contact){
//     return this.http.post(environment.rooturl + AppConstants.CONTACT_API_URL, contactMessage, { observe: 'response'});
//   }

// }

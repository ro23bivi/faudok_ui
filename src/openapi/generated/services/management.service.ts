/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { deleteUserById } from '../fn/management/delete-user-by-id';
import { DeleteUserById$Params } from '../fn/management/delete-user-by-id';
import { FaudokResponsePageFaudokUserInfoResponseDto } from '../models/faudok-response-page-faudok-user-info-response-dto';
import { FaudokUserInfoResponseDto } from '../models/faudok-user-info-response-dto';
import { getAccountDetails } from '../fn/management/get-account-details';
import { GetAccountDetails$Params } from '../fn/management/get-account-details';
import { getRole } from '../fn/management/get-role';
import { GetRole$Params } from '../fn/management/get-role';
import { getUserByEmail } from '../fn/management/get-user-by-email';
import { GetUserByEmail$Params } from '../fn/management/get-user-by-email';
import { getUserById } from '../fn/management/get-user-by-id';
import { GetUserById$Params } from '../fn/management/get-user-by-id';
import { getUsers } from '../fn/management/get-users';
import { GetUsers$Params } from '../fn/management/get-users';
import { register } from '../fn/management/register';
import { Register$Params } from '../fn/management/register';
import { resetPasswordRequestById } from '../fn/management/reset-password-request-by-id';
import { ResetPasswordRequestById$Params } from '../fn/management/reset-password-request-by-id';
import { unlockUserById } from '../fn/management/unlock-user-by-id';
import { UnlockUserById$Params } from '../fn/management/unlock-user-by-id';
import { updateFaudokUser } from '../fn/management/update-faudok-user';
import { UpdateFaudokUser$Params } from '../fn/management/update-faudok-user';
import { updateFaudokUser1 } from '../fn/management/update-faudok-user-1';
import { UpdateFaudokUser1$Params } from '../fn/management/update-faudok-user-1';

@Injectable({ providedIn: 'root' })
export class ManagementService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `getUserById()` */
  static readonly GetUserByIdPath = '/v1/management/users/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUserById()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserById$Response(params: GetUserById$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokUserInfoResponseDto>> {
    return getUserById(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUserById$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserById(params: GetUserById$Params, context?: HttpContext): Observable<FaudokUserInfoResponseDto> {
    return this.getUserById$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokUserInfoResponseDto>): FaudokUserInfoResponseDto => r.body)
    );
  }

  /** Path part for operation `updateFaudokUser()` */
  static readonly UpdateFaudokUserPath = '/v1/management/users/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateFaudokUser()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateFaudokUser$Response(params: UpdateFaudokUser$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return updateFaudokUser(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `updateFaudokUser$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateFaudokUser(params: UpdateFaudokUser$Params, context?: HttpContext): Observable<void> {
    return this.updateFaudokUser$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `deleteUserById()` */
  static readonly DeleteUserByIdPath = '/v1/management/users/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteUserById()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUserById$Response(params: DeleteUserById$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return deleteUserById(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `deleteUserById$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUserById(params: DeleteUserById$Params, context?: HttpContext): Observable<void> {
    return this.deleteUserById$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `unlockUserById()` */
  static readonly UnlockUserByIdPath = '/v1/management/users/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `unlockUserById()` instead.
   *
   * This method doesn't expect any request body.
   */
  unlockUserById$Response(params: UnlockUserById$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return unlockUserById(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `unlockUserById$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  unlockUserById(params: UnlockUserById$Params, context?: HttpContext): Observable<void> {
    return this.unlockUserById$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `resetPasswordRequestById()` */
  static readonly ResetPasswordRequestByIdPath = '/v1/management/users/{id}/reset';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `resetPasswordRequestById()` instead.
   *
   * This method doesn't expect any request body.
   */
  resetPasswordRequestById$Response(params: ResetPasswordRequestById$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return resetPasswordRequestById(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `resetPasswordRequestById$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  resetPasswordRequestById(params: ResetPasswordRequestById$Params, context?: HttpContext): Observable<void> {
    return this.resetPasswordRequestById$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `register()` */
  static readonly RegisterPath = '/v1/management/users/register';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `register()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  register$Response(params: Register$Params, context?: HttpContext): Observable<StrictHttpResponse<{
}>> {
    return register(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `register$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  register(params: Register$Params, context?: HttpContext): Observable<{
}> {
    return this.register$Response(params, context).pipe(
      map((r: StrictHttpResponse<{
}>): {
} => r.body)
    );
  }

  /** Path part for operation `updateFaudokUser1()` */
  static readonly UpdateFaudokUser1Path = '/v1/management/users/update';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateFaudokUser1()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateFaudokUser1$Response(params: UpdateFaudokUser1$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return updateFaudokUser1(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `updateFaudokUser1$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateFaudokUser1(params: UpdateFaudokUser1$Params, context?: HttpContext): Observable<void> {
    return this.updateFaudokUser1$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `getUsers()` */
  static readonly GetUsersPath = '/v1/management/users';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUsers()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUsers$Response(params?: GetUsers$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageFaudokUserInfoResponseDto>> {
    return getUsers(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUsers$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUsers(params?: GetUsers$Params, context?: HttpContext): Observable<FaudokResponsePageFaudokUserInfoResponseDto> {
    return this.getUsers$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokResponsePageFaudokUserInfoResponseDto>): FaudokResponsePageFaudokUserInfoResponseDto => r.body)
    );
  }

  /** Path part for operation `getUserByEmail()` */
  static readonly GetUserByEmailPath = '/v1/management/users/{email}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUserByEmail()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserByEmail$Response(params: GetUserByEmail$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokUserInfoResponseDto>> {
    return getUserByEmail(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUserByEmail$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserByEmail(params: GetUserByEmail$Params, context?: HttpContext): Observable<FaudokUserInfoResponseDto> {
    return this.getUserByEmail$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokUserInfoResponseDto>): FaudokUserInfoResponseDto => r.body)
    );
  }

  /** Path part for operation `getAccountDetails()` */
  static readonly GetAccountDetailsPath = '/v1/management/users/userinfo';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAccountDetails()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAccountDetails$Response(params?: GetAccountDetails$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokUserInfoResponseDto>> {
    return getAccountDetails(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getAccountDetails$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAccountDetails(params?: GetAccountDetails$Params, context?: HttpContext): Observable<FaudokUserInfoResponseDto> {
    return this.getAccountDetails$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokUserInfoResponseDto>): FaudokUserInfoResponseDto => r.body)
    );
  }

  /** Path part for operation `getRole()` */
  static readonly GetRolePath = '/v1/management/users/role';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getRole()` instead.
   *
   * This method doesn't expect any request body.
   */
  getRole$Response(params?: GetRole$Params, context?: HttpContext): Observable<StrictHttpResponse<string>> {
    return getRole(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getRole$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getRole(params?: GetRole$Params, context?: HttpContext): Observable<string> {
    return this.getRole$Response(params, context).pipe(
      map((r: StrictHttpResponse<string>): string => r.body)
    );
  }

}

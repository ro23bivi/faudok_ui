/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { assignUnownedOrder } from '../fn/order/assign-unowned-order';
import { AssignUnownedOrder$Params } from '../fn/order/assign-unowned-order';
import { FaudokResponsePageOrderResponseDto } from '../models/faudok-response-page-order-response-dto';
import { getChairsArchivedOrders } from '../fn/order/get-chairs-archived-orders';
import { GetChairsArchivedOrders$Params } from '../fn/order/get-chairs-archived-orders';
import { getChairsAvailableOrders } from '../fn/order/get-chairs-available-orders';
import { GetChairsAvailableOrders$Params } from '../fn/order/get-chairs-available-orders';
import { getChairsOrderByClientEmail } from '../fn/order/get-chairs-order-by-client-email';
import { GetChairsOrderByClientEmail$Params } from '../fn/order/get-chairs-order-by-client-email';
import { getChairsOrderById } from '../fn/order/get-chairs-order-by-id';
import { GetChairsOrderById$Params } from '../fn/order/get-chairs-order-by-id';
import { getChairsOrderBySignature } from '../fn/order/get-chairs-order-by-signature';
import { GetChairsOrderBySignature$Params } from '../fn/order/get-chairs-order-by-signature';
import { getUsersArchivedOrders } from '../fn/order/get-users-archived-orders';
import { GetUsersArchivedOrders$Params } from '../fn/order/get-users-archived-orders';
import { getUsersAvailableOrders } from '../fn/order/get-users-available-orders';
import { GetUsersAvailableOrders$Params } from '../fn/order/get-users-available-orders';
import { handleRejectOrder } from '../fn/order/handle-reject-order';
import { HandleRejectOrder$Params } from '../fn/order/handle-reject-order';
import { handleRescanOrder } from '../fn/order/handle-rescan-order';
import { HandleRescanOrder$Params } from '../fn/order/handle-rescan-order';
import { handleScanOrder } from '../fn/order/handle-scan-order';
import { HandleScanOrder$Params } from '../fn/order/handle-scan-order';
import { OrderResponseDto } from '../models/order-response-dto';
import { unassignOwnedOrder } from '../fn/order/unassign-owned-order';
import { UnassignOwnedOrder$Params } from '../fn/order/unassign-owned-order';

@Injectable({ providedIn: 'root' })
export class OrderService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `unassignOwnedOrder()` */
  static readonly UnassignOwnedOrderPath = '/v1/orders/unassign/{orderId}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `unassignOwnedOrder()` instead.
   *
   * This method doesn't expect any request body.
   */
  unassignOwnedOrder$Response(params: UnassignOwnedOrder$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return unassignOwnedOrder(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `unassignOwnedOrder$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  unassignOwnedOrder(params: UnassignOwnedOrder$Params, context?: HttpContext): Observable<void> {
    return this.unassignOwnedOrder$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `handleScanOrder()` */
  static readonly HandleScanOrderPath = '/v1/orders/scan/{orderId}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `handleScanOrder()` instead.
   *
   * This method doesn't expect any request body.
   */
  handleScanOrder$Response(params: HandleScanOrder$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return handleScanOrder(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `handleScanOrder$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  handleScanOrder(params: HandleScanOrder$Params, context?: HttpContext): Observable<void> {
    return this.handleScanOrder$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `handleRescanOrder()` */
  static readonly HandleRescanOrderPath = '/v1/orders/rescan/{orderId}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `handleRescanOrder()` instead.
   *
   * This method doesn't expect any request body.
   */
  handleRescanOrder$Response(params: HandleRescanOrder$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return handleRescanOrder(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `handleRescanOrder$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  handleRescanOrder(params: HandleRescanOrder$Params, context?: HttpContext): Observable<void> {
    return this.handleRescanOrder$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `handleRejectOrder()` */
  static readonly HandleRejectOrderPath = '/v1/orders/reject/{orderId}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `handleRejectOrder()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  handleRejectOrder$Response(params: HandleRejectOrder$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return handleRejectOrder(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `handleRejectOrder$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  handleRejectOrder(params: HandleRejectOrder$Params, context?: HttpContext): Observable<void> {
    return this.handleRejectOrder$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `assignUnownedOrder()` */
  static readonly AssignUnownedOrderPath = '/v1/orders/assign/{orderId}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `assignUnownedOrder()` instead.
   *
   * This method doesn't expect any request body.
   */
  assignUnownedOrder$Response(params: AssignUnownedOrder$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return assignUnownedOrder(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `assignUnownedOrder$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  assignUnownedOrder(params: AssignUnownedOrder$Params, context?: HttpContext): Observable<void> {
    return this.assignUnownedOrder$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `getChairsOrderBySignature()` */
  static readonly GetChairsOrderBySignaturePath = '/v1/orders/signature/{orderSignature}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getChairsOrderBySignature()` instead.
   *
   * This method doesn't expect any request body.
   */
  getChairsOrderBySignature$Response(params: GetChairsOrderBySignature$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
    return getChairsOrderBySignature(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getChairsOrderBySignature$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getChairsOrderBySignature(params: GetChairsOrderBySignature$Params, context?: HttpContext): Observable<FaudokResponsePageOrderResponseDto> {
    return this.getChairsOrderBySignature$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokResponsePageOrderResponseDto>): FaudokResponsePageOrderResponseDto => r.body)
    );
  }

  /** Path part for operation `getChairsOrderById()` */
  static readonly GetChairsOrderByIdPath = '/v1/orders/id/{orderId}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getChairsOrderById()` instead.
   *
   * This method doesn't expect any request body.
   */
  getChairsOrderById$Response(params: GetChairsOrderById$Params, context?: HttpContext): Observable<StrictHttpResponse<OrderResponseDto>> {
    return getChairsOrderById(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getChairsOrderById$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getChairsOrderById(params: GetChairsOrderById$Params, context?: HttpContext): Observable<OrderResponseDto> {
    return this.getChairsOrderById$Response(params, context).pipe(
      map((r: StrictHttpResponse<OrderResponseDto>): OrderResponseDto => r.body)
    );
  }

  /** Path part for operation `getChairsOrderByClientEmail()` */
  static readonly GetChairsOrderByClientEmailPath = '/v1/orders/email/{orderSignature}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getChairsOrderByClientEmail()` instead.
   *
   * This method doesn't expect any request body.
   */
  getChairsOrderByClientEmail$Response(params?: GetChairsOrderByClientEmail$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
    return getChairsOrderByClientEmail(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getChairsOrderByClientEmail$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getChairsOrderByClientEmail(params?: GetChairsOrderByClientEmail$Params, context?: HttpContext): Observable<FaudokResponsePageOrderResponseDto> {
    return this.getChairsOrderByClientEmail$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokResponsePageOrderResponseDto>): FaudokResponsePageOrderResponseDto => r.body)
    );
  }

  /** Path part for operation `getUsersAvailableOrders()` */
  static readonly GetUsersAvailableOrdersPath = '/v1/orders/available';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUsersAvailableOrders()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUsersAvailableOrders$Response(params?: GetUsersAvailableOrders$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
    return getUsersAvailableOrders(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUsersAvailableOrders$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUsersAvailableOrders(params?: GetUsersAvailableOrders$Params, context?: HttpContext): Observable<FaudokResponsePageOrderResponseDto> {
    return this.getUsersAvailableOrders$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokResponsePageOrderResponseDto>): FaudokResponsePageOrderResponseDto => r.body)
    );
  }

  /** Path part for operation `getChairsAvailableOrders()` */
  static readonly GetChairsAvailableOrdersPath = '/v1/orders/available/chair';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getChairsAvailableOrders()` instead.
   *
   * This method doesn't expect any request body.
   */
  getChairsAvailableOrders$Response(params?: GetChairsAvailableOrders$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
    return getChairsAvailableOrders(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getChairsAvailableOrders$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getChairsAvailableOrders(params?: GetChairsAvailableOrders$Params, context?: HttpContext): Observable<FaudokResponsePageOrderResponseDto> {
    return this.getChairsAvailableOrders$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokResponsePageOrderResponseDto>): FaudokResponsePageOrderResponseDto => r.body)
    );
  }

  /** Path part for operation `getUsersArchivedOrders()` */
  static readonly GetUsersArchivedOrdersPath = '/v1/orders/archived';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUsersArchivedOrders()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUsersArchivedOrders$Response(params?: GetUsersArchivedOrders$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
    return getUsersArchivedOrders(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUsersArchivedOrders$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUsersArchivedOrders(params?: GetUsersArchivedOrders$Params, context?: HttpContext): Observable<FaudokResponsePageOrderResponseDto> {
    return this.getUsersArchivedOrders$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokResponsePageOrderResponseDto>): FaudokResponsePageOrderResponseDto => r.body)
    );
  }

  /** Path part for operation `getChairsArchivedOrders()` */
  static readonly GetChairsArchivedOrdersPath = '/v1/orders/archived/chair';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getChairsArchivedOrders()` instead.
   *
   * This method doesn't expect any request body.
   */
  getChairsArchivedOrders$Response(params?: GetChairsArchivedOrders$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
    return getChairsArchivedOrders(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getChairsArchivedOrders$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getChairsArchivedOrders(params?: GetChairsArchivedOrders$Params, context?: HttpContext): Observable<FaudokResponsePageOrderResponseDto> {
    return this.getChairsArchivedOrders$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokResponsePageOrderResponseDto>): FaudokResponsePageOrderResponseDto => r.body)
    );
  }

}

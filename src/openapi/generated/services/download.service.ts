/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { downloadFile1 } from '../fn/download/download-file-1';
import { DownloadFile1$Params } from '../fn/download/download-file-1';

@Injectable({ providedIn: 'root' })
export class DownloadService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `downloadFile1()` */
  static readonly DownloadFile1Path = '/download/{scanId}/{orderId}/{fileName}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `downloadFile1()` instead.
   *
   * This method doesn't expect any request body.
   */
  downloadFile1$Response(params: DownloadFile1$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return downloadFile1(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `downloadFile1$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  downloadFile1(params: DownloadFile1$Params, context?: HttpContext): Observable<Blob> {
    return this.downloadFile1$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

}

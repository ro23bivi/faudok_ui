/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { downloadFile } from '../fn/admin-order/download-file';
import { DownloadFile$Params } from '../fn/admin-order/download-file';
import { FaudokResponsePageOrderResponseDto } from '../models/faudok-response-page-order-response-dto';
import { getAllArchivedOrders } from '../fn/admin-order/get-all-archived-orders';
import { GetAllArchivedOrders$Params } from '../fn/admin-order/get-all-archived-orders';
import { getAllAvailableOrders } from '../fn/admin-order/get-all-available-orders';
import { GetAllAvailableOrders$Params } from '../fn/admin-order/get-all-available-orders';
import { getAllDeletedOrders } from '../fn/admin-order/get-all-deleted-orders';
import { GetAllDeletedOrders$Params } from '../fn/admin-order/get-all-deleted-orders';
import { getAnyOrderById } from '../fn/admin-order/get-any-order-by-id';
import { GetAnyOrderById$Params } from '../fn/admin-order/get-any-order-by-id';
import { getAnyOrderBySignature } from '../fn/admin-order/get-any-order-by-signature';
import { GetAnyOrderBySignature$Params } from '../fn/admin-order/get-any-order-by-signature';
import { getAnyOrdersByClientEmail } from '../fn/admin-order/get-any-orders-by-client-email';
import { GetAnyOrdersByClientEmail$Params } from '../fn/admin-order/get-any-orders-by-client-email';
import { getChairsDeletedOrders } from '../fn/admin-order/get-chairs-deleted-orders';
import { GetChairsDeletedOrders$Params } from '../fn/admin-order/get-chairs-deleted-orders';
import { moveOrder } from '../fn/admin-order/move-order';
import { MoveOrder$Params } from '../fn/admin-order/move-order';
import { OrderResponseDto } from '../models/order-response-dto';
import { purgeOrder } from '../fn/admin-order/purge-order';
import { PurgeOrder$Params } from '../fn/admin-order/purge-order';
import { reassignOrderFromOtherUser } from '../fn/admin-order/reassign-order-from-other-user';
import { ReassignOrderFromOtherUser$Params } from '../fn/admin-order/reassign-order-from-other-user';
import { setDeleted } from '../fn/admin-order/set-deleted';
import { SetDeleted$Params } from '../fn/admin-order/set-deleted';
import { unassignAnyOrder } from '../fn/admin-order/unassign-any-order';
import { UnassignAnyOrder$Params } from '../fn/admin-order/unassign-any-order';
import { updateAnyOrdersMetadata } from '../fn/admin-order/update-any-orders-metadata';
import { UpdateAnyOrdersMetadata$Params } from '../fn/admin-order/update-any-orders-metadata';

@Injectable({ providedIn: 'root' })
export class AdminOrderService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `updateAnyOrdersMetadata()` */
  static readonly UpdateAnyOrdersMetadataPath = '/v1/management/orders/update/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateAnyOrdersMetadata()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateAnyOrdersMetadata$Response(params: UpdateAnyOrdersMetadata$Params, context?: HttpContext): Observable<StrictHttpResponse<OrderResponseDto>> {
    return updateAnyOrdersMetadata(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `updateAnyOrdersMetadata$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateAnyOrdersMetadata(params: UpdateAnyOrdersMetadata$Params, context?: HttpContext): Observable<OrderResponseDto> {
    return this.updateAnyOrdersMetadata$Response(params, context).pipe(
      map((r: StrictHttpResponse<OrderResponseDto>): OrderResponseDto => r.body)
    );
  }

  /** Path part for operation `moveOrder()` */
  static readonly MoveOrderPath = '/v1/management/orders/move/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `moveOrder()` instead.
   *
   * This method doesn't expect any request body.
   */
  moveOrder$Response(params: MoveOrder$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return moveOrder(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `moveOrder$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  moveOrder(params: MoveOrder$Params, context?: HttpContext): Observable<void> {
    return this.moveOrder$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `reassignOrderFromOtherUser()` */
  static readonly ReassignOrderFromOtherUserPath = '/v1/management/orders/reassign/{orderId}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `reassignOrderFromOtherUser()` instead.
   *
   * This method doesn't expect any request body.
   */
  reassignOrderFromOtherUser$Response(params: ReassignOrderFromOtherUser$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return reassignOrderFromOtherUser(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `reassignOrderFromOtherUser$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  reassignOrderFromOtherUser(params: ReassignOrderFromOtherUser$Params, context?: HttpContext): Observable<void> {
    return this.reassignOrderFromOtherUser$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `unassignAnyOrder()` */
  static readonly UnassignAnyOrderPath = '/v1/management/orders/admin/unassign/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `unassignAnyOrder()` instead.
   *
   * This method doesn't expect any request body.
   */
  unassignAnyOrder$Response(params: UnassignAnyOrder$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return unassignAnyOrder(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `unassignAnyOrder$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  unassignAnyOrder(params: UnassignAnyOrder$Params, context?: HttpContext): Observable<void> {
    return this.unassignAnyOrder$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `getAnyOrderBySignature()` */
  static readonly GetAnyOrderBySignaturePath = '/v1/management/orders/signature/{orderSignature}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAnyOrderBySignature()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAnyOrderBySignature$Response(params: GetAnyOrderBySignature$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
    return getAnyOrderBySignature(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getAnyOrderBySignature$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAnyOrderBySignature(params: GetAnyOrderBySignature$Params, context?: HttpContext): Observable<FaudokResponsePageOrderResponseDto> {
    return this.getAnyOrderBySignature$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokResponsePageOrderResponseDto>): FaudokResponsePageOrderResponseDto => r.body)
    );
  }

  /** Path part for operation `getAnyOrderById()` */
  static readonly GetAnyOrderByIdPath = '/v1/management/orders/id/{orderId}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAnyOrderById()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAnyOrderById$Response(params: GetAnyOrderById$Params, context?: HttpContext): Observable<StrictHttpResponse<OrderResponseDto>> {
    return getAnyOrderById(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getAnyOrderById$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAnyOrderById(params: GetAnyOrderById$Params, context?: HttpContext): Observable<OrderResponseDto> {
    return this.getAnyOrderById$Response(params, context).pipe(
      map((r: StrictHttpResponse<OrderResponseDto>): OrderResponseDto => r.body)
    );
  }

  /** Path part for operation `downloadFile()` */
  static readonly DownloadFilePath = '/v1/management/orders/files/{fileName}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `downloadFile()` instead.
   *
   * This method doesn't expect any request body.
   */
  downloadFile$Response(params: DownloadFile$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return downloadFile(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `downloadFile$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  downloadFile(params: DownloadFile$Params, context?: HttpContext): Observable<Blob> {
    return this.downloadFile$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

  /** Path part for operation `getAnyOrdersByClientEmail()` */
  static readonly GetAnyOrdersByClientEmailPath = '/v1/management/orders/email/{clientEmail}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAnyOrdersByClientEmail()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAnyOrdersByClientEmail$Response(params: GetAnyOrdersByClientEmail$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
    return getAnyOrdersByClientEmail(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getAnyOrdersByClientEmail$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAnyOrdersByClientEmail(params: GetAnyOrdersByClientEmail$Params, context?: HttpContext): Observable<FaudokResponsePageOrderResponseDto> {
    return this.getAnyOrdersByClientEmail$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokResponsePageOrderResponseDto>): FaudokResponsePageOrderResponseDto => r.body)
    );
  }

  /** Path part for operation `getAllDeletedOrders()` */
  static readonly GetAllDeletedOrdersPath = '/v1/management/orders/deleted';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllDeletedOrders()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllDeletedOrders$Response(params?: GetAllDeletedOrders$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
    return getAllDeletedOrders(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getAllDeletedOrders$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllDeletedOrders(params?: GetAllDeletedOrders$Params, context?: HttpContext): Observable<FaudokResponsePageOrderResponseDto> {
    return this.getAllDeletedOrders$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokResponsePageOrderResponseDto>): FaudokResponsePageOrderResponseDto => r.body)
    );
  }

  /** Path part for operation `getChairsDeletedOrders()` */
  static readonly GetChairsDeletedOrdersPath = '/v1/management/orders/deleted/chair';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getChairsDeletedOrders()` instead.
   *
   * This method doesn't expect any request body.
   */
  getChairsDeletedOrders$Response(params?: GetChairsDeletedOrders$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
    return getChairsDeletedOrders(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getChairsDeletedOrders$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getChairsDeletedOrders(params?: GetChairsDeletedOrders$Params, context?: HttpContext): Observable<FaudokResponsePageOrderResponseDto> {
    return this.getChairsDeletedOrders$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokResponsePageOrderResponseDto>): FaudokResponsePageOrderResponseDto => r.body)
    );
  }

  /** Path part for operation `getAllAvailableOrders()` */
  static readonly GetAllAvailableOrdersPath = '/v1/management/orders/available';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllAvailableOrders()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllAvailableOrders$Response(params?: GetAllAvailableOrders$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
    return getAllAvailableOrders(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getAllAvailableOrders$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllAvailableOrders(params?: GetAllAvailableOrders$Params, context?: HttpContext): Observable<FaudokResponsePageOrderResponseDto> {
    return this.getAllAvailableOrders$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokResponsePageOrderResponseDto>): FaudokResponsePageOrderResponseDto => r.body)
    );
  }

  /** Path part for operation `getAllArchivedOrders()` */
  static readonly GetAllArchivedOrdersPath = '/v1/management/orders/archived';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllArchivedOrders()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllArchivedOrders$Response(params?: GetAllArchivedOrders$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
    return getAllArchivedOrders(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getAllArchivedOrders$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllArchivedOrders(params?: GetAllArchivedOrders$Params, context?: HttpContext): Observable<FaudokResponsePageOrderResponseDto> {
    return this.getAllArchivedOrders$Response(params, context).pipe(
      map((r: StrictHttpResponse<FaudokResponsePageOrderResponseDto>): FaudokResponsePageOrderResponseDto => r.body)
    );
  }

  /** Path part for operation `purgeOrder()` */
  static readonly PurgeOrderPath = '/v1/management/orders/purge/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `purgeOrder()` instead.
   *
   * This method doesn't expect any request body.
   */
  purgeOrder$Response(params: PurgeOrder$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return purgeOrder(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `purgeOrder$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  purgeOrder(params: PurgeOrder$Params, context?: HttpContext): Observable<void> {
    return this.purgeOrder$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `setDeleted()` */
  static readonly SetDeletedPath = '/v1/management/orders/delete/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `setDeleted()` instead.
   *
   * This method doesn't expect any request body.
   */
  setDeleted$Response(params: SetDeleted$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return setDeleted(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `setDeleted$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  setDeleted(params: SetDeleted$Params, context?: HttpContext): Observable<void> {
    return this.setDeleted$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

}

/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { sendContactRequest } from '../fn/contact/send-contact-request';
import { SendContactRequest$Params } from '../fn/contact/send-contact-request';

@Injectable({ providedIn: 'root' })
export class ContactService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `sendContactRequest()` */
  static readonly SendContactRequestPath = '/v1/contact';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `sendContactRequest()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  sendContactRequest$Response(params: SendContactRequest$Params, context?: HttpContext): Observable<StrictHttpResponse<{
}>> {
    return sendContactRequest(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `sendContactRequest$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  sendContactRequest(params: SendContactRequest$Params, context?: HttpContext): Observable<{
}> {
    return this.sendContactRequest$Response(params, context).pipe(
      map((r: StrictHttpResponse<{
}>): {
} => r.body)
    );
  }

}

/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { confirm } from '../fn/authentication/confirm';
import { Confirm$Params } from '../fn/authentication/confirm';
import { login } from '../fn/authentication/login';
import { Login$Params } from '../fn/authentication/login';
import { LoginResponseDto } from '../models/login-response-dto';
import { resetPasswordHandler } from '../fn/authentication/reset-password-handler';
import { ResetPasswordHandler$Params } from '../fn/authentication/reset-password-handler';
import { resetPasswordRequestEmail } from '../fn/authentication/reset-password-request-email';
import { ResetPasswordRequestEmail$Params } from '../fn/authentication/reset-password-request-email';

@Injectable({ providedIn: 'root' })
export class AuthenticationService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `resetPasswordHandler()` */
  static readonly ResetPasswordHandlerPath = '/v1/auth/reset/update';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `resetPasswordHandler()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  resetPasswordHandler$Response(params: ResetPasswordHandler$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return resetPasswordHandler(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `resetPasswordHandler$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  resetPasswordHandler(params: ResetPasswordHandler$Params, context?: HttpContext): Observable<void> {
    return this.resetPasswordHandler$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `resetPasswordRequestEmail()` */
  static readonly ResetPasswordRequestEmailPath = '/v1/auth/reset/email';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `resetPasswordRequestEmail()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  resetPasswordRequestEmail$Response(params: ResetPasswordRequestEmail$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return resetPasswordRequestEmail(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `resetPasswordRequestEmail$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  resetPasswordRequestEmail(params: ResetPasswordRequestEmail$Params, context?: HttpContext): Observable<void> {
    return this.resetPasswordRequestEmail$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `login()` */
  static readonly LoginPath = '/v1/auth/login';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `login()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  login$Response(params: Login$Params, context?: HttpContext): Observable<StrictHttpResponse<LoginResponseDto>> {
    return login(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `login$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  login(params: Login$Params, context?: HttpContext): Observable<LoginResponseDto> {
    return this.login$Response(params, context).pipe(
      map((r: StrictHttpResponse<LoginResponseDto>): LoginResponseDto => r.body)
    );
  }

  /** Path part for operation `confirm()` */
  static readonly ConfirmPath = '/v1/auth/activate';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `confirm()` instead.
   *
   * This method doesn't expect any request body.
   */
  confirm$Response(params: Confirm$Params, context?: HttpContext): Observable<StrictHttpResponse<{
}>> {
    return confirm(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `confirm$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  confirm(params: Confirm$Params, context?: HttpContext): Observable<{
}> {
    return this.confirm$Response(params, context).pipe(
      map((r: StrictHttpResponse<{
}>): {
} => r.body)
    );
  }

}

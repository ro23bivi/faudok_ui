/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { FaudokUserInfoResponseDto } from '../../models/faudok-user-info-response-dto';

export interface GetAccountDetails$Params {
}

export function getAccountDetails(http: HttpClient, rootUrl: string, params?: GetAccountDetails$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokUserInfoResponseDto>> {
  const rb = new RequestBuilder(rootUrl, getAccountDetails.PATH, 'get');
  if (params) {
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<FaudokUserInfoResponseDto>;
    })
  );
}

getAccountDetails.PATH = '/v1/management/users/userinfo';

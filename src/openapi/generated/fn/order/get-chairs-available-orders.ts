/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { FaudokResponsePageOrderResponseDto } from '../../models/faudok-response-page-order-response-dto';

export interface GetChairsAvailableOrders$Params {
  pageNo?: number;
  pageSize?: number;
  sortBy?: string;
  sortDir?: string;
}

export function getChairsAvailableOrders(http: HttpClient, rootUrl: string, params?: GetChairsAvailableOrders$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
  const rb = new RequestBuilder(rootUrl, getChairsAvailableOrders.PATH, 'get');
  if (params) {
    rb.query('pageNo', params.pageNo, {});
    rb.query('pageSize', params.pageSize, {});
    rb.query('sortBy', params.sortBy, {});
    rb.query('sortDir', params.sortDir, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<FaudokResponsePageOrderResponseDto>;
    })
  );
}

getChairsAvailableOrders.PATH = '/v1/orders/available/chair';

/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { FaudokResponsePageOrderResponseDto } from '../../models/faudok-response-page-order-response-dto';

export interface GetChairsOrderByClientEmail$Params {
  clientEmail: string;
  pageNo?: number;
  pageSize?: number;
  sortBy?: string;
  sortDir?: string;
}

export function getChairsOrderByClientEmail(http: HttpClient, rootUrl: string, params?: GetChairsOrderByClientEmail$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
  const rb = new RequestBuilder(rootUrl, getChairsOrderByClientEmail.PATH, 'get');
  if (params) {
    rb.query('pageNo', params.pageNo, {});
    rb.query('pageSize', params.pageSize, {});
    rb.query('sortBy', params.sortBy, {});
    rb.query('sortDir', params.sortDir, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<FaudokResponsePageOrderResponseDto>;
    })
  );
}

getChairsOrderByClientEmail.PATH = '/v1/orders/email/{orderSignature}';

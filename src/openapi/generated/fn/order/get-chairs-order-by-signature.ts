/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { FaudokResponsePageOrderResponseDto } from '../../models/faudok-response-page-order-response-dto';

export interface GetChairsOrderBySignature$Params {
  orderSignature: string;
  pageNo?: number;
  pageSize?: number;
  sortBy?: string;
  sortDir?: string;
}

export function getChairsOrderBySignature(http: HttpClient, rootUrl: string, params: GetChairsOrderBySignature$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
  const rb = new RequestBuilder(rootUrl, getChairsOrderBySignature.PATH, 'get');
  if (params) {
    rb.path('orderSignature', params.orderSignature, {});
    rb.query('pageNo', params.pageNo, {});
    rb.query('pageSize', params.pageSize, {});
    rb.query('sortBy', params.sortBy, {});
    rb.query('sortDir', params.sortDir, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<FaudokResponsePageOrderResponseDto>;
    })
  );
}

getChairsOrderBySignature.PATH = '/v1/orders/signature/{orderSignature}';

/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { OrderResponseDto } from '../../models/order-response-dto';

export interface GetAnyOrderById$Params {
  orderId: string;
}

export function getAnyOrderById(http: HttpClient, rootUrl: string, params: GetAnyOrderById$Params, context?: HttpContext): Observable<StrictHttpResponse<OrderResponseDto>> {
  const rb = new RequestBuilder(rootUrl, getAnyOrderById.PATH, 'get');
  if (params) {
    rb.path('orderId', params.orderId, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<OrderResponseDto>;
    })
  );
}

getAnyOrderById.PATH = '/v1/management/orders/id/{orderId}';

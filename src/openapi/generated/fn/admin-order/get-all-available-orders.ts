/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { FaudokResponsePageOrderResponseDto } from '../../models/faudok-response-page-order-response-dto';

export interface GetAllAvailableOrders$Params {
  pageNo?: number;
  pageSize?: number;
  sortBy?: string;
  sortDir?: string;
}

export function getAllAvailableOrders(http: HttpClient, rootUrl: string, params?: GetAllAvailableOrders$Params, context?: HttpContext): Observable<StrictHttpResponse<FaudokResponsePageOrderResponseDto>> {
  const rb = new RequestBuilder(rootUrl, getAllAvailableOrders.PATH, 'get');
  if (params) {
    rb.query('pageNo', params.pageNo, {});
    rb.query('pageSize', params.pageSize, {});
    rb.query('sortBy', params.sortBy, {});
    rb.query('sortDir', params.sortDir, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<FaudokResponsePageOrderResponseDto>;
    })
  );
}

getAllAvailableOrders.PATH = '/v1/management/orders/available';

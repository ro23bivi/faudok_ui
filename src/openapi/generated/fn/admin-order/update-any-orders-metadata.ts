/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { OrderRequestDto } from '../../models/order-request-dto';
import { OrderResponseDto } from '../../models/order-response-dto';

export interface UpdateAnyOrdersMetadata$Params {
  id: string;
      body: OrderRequestDto
}

export function updateAnyOrdersMetadata(http: HttpClient, rootUrl: string, params: UpdateAnyOrdersMetadata$Params, context?: HttpContext): Observable<StrictHttpResponse<OrderResponseDto>> {
  const rb = new RequestBuilder(rootUrl, updateAnyOrdersMetadata.PATH, 'put');
  if (params) {
    rb.path('id', params.id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<OrderResponseDto>;
    })
  );
}

updateAnyOrdersMetadata.PATH = '/v1/management/orders/update/{id}';

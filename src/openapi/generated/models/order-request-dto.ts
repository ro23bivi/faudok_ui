/* tslint:disable */
/* eslint-disable */
export interface OrderRequestDto {
  authorArticle?: string;
  authorBook?: string;
  bvNr?: string;
  isbn?: string;
  issn?: string;
  issue?: string;
  issueArticle?: string;
  pages: string;
  placePublication?: string;
  publicationDate?: string;
  publisher?: string;
  signature: string;
  titleArticle?: string;
  titleBook?: string;
  type?: string;
}

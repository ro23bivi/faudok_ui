/* tslint:disable */
/* eslint-disable */
export interface FaudokAdminUserUpdateRequestDto {
  accountLocked?: boolean;
  faculty?: string;
  name: string;
  role?: string;
}

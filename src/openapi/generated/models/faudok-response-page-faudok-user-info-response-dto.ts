/* tslint:disable */
/* eslint-disable */
import { FaudokUserInfoResponseDto } from '../models/faudok-user-info-response-dto';
export interface FaudokResponsePageFaudokUserInfoResponseDto {
  content?: Array<FaudokUserInfoResponseDto>;
  first?: boolean;
  last?: boolean;
  pageNo?: number;
  pageSize?: number;
  totalElements?: number;
  totalPages?: number;
}

/* tslint:disable */
/* eslint-disable */
export interface FaudokUserInfoResponseDto {
  accountLocked?: boolean;
  email: string;
  faculty?: string;
  name: string;
  role?: string;
}

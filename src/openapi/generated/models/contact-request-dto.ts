/* tslint:disable */
/* eslint-disable */
export interface ContactRequestDto {
  email: string;
  message: string;
  subject: string;
}

/* tslint:disable */
/* eslint-disable */
import { OrderResponseDto } from '../models/order-response-dto';
export interface FaudokResponsePageOrderResponseDto {
  content?: Array<OrderResponseDto>;
  first?: boolean;
  last?: boolean;
  pageNo?: number;
  pageSize?: number;
  totalElements?: number;
  totalPages?: number;
}

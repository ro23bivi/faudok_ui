/* tslint:disable */
/* eslint-disable */
export interface RegisterRequestDto {
  email: string;
  faculty?: string;
  name: string;
  password: string;
  role?: string;
}

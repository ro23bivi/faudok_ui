/* tslint:disable */
/* eslint-disable */
export interface OrderResponseDto {
  authorArticle?: string;
  authorBook?: string;
  bvNr?: string;
  chair?: string;
  clientDeliveryEmail: string;
  clientDeliveryName: string;
  deleted?: boolean;
  doneDate?: string;
  dueDate?: string;
  faudokWorkerEmail?: string;
  id?: string;
  isbn?: string;
  issn?: string;
  issue?: string;
  issueArticle?: string;
  pages: string;
  placePublication?: string;
  publicationDate?: string;
  publisher?: string;
  reasonRejected?: string;
  rejected?: boolean;
  scanPath?: string;
  scanned?: boolean;
  signature: string;
  state?: string;
  titleArticle?: string;
  titleBook?: string;
  transactionGroupQualifier?: string;
  type?: string;
  userGroup?: string;
}

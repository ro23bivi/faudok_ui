/* tslint:disable */
/* eslint-disable */
export interface PasswordUpdateRequestDto {
  code: string;
  password: string;
}

### STAGE 1: Build ###
FROM node:18-alpine AS build
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY . .
#RUN npm run build
RUN npm install @angular/cli -g
RUN ng build
CMD ng serve --host 0.0.0.0 --port 4200
#RUN npm install --global pm2
#RUN pm2 start "ng serve"


### STAGE 2: Run with nginx ###
# FROM nginx:1.17.1-alpine
# COPY default.conf /etc/nginx/conf.d/default.conf
# COPY --from=build /usr/src/app/dist/faudok /usr/share/nginx/html
# EXPOSE 80

### STAGE 2: Run with Apache2 and HTTPS ###
# FROM httpd:2.4
# RUN a2enmod proxy
# RUN a2enmod proxy_http
# RUN a2enmod ssl
# RUN sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt
# COPY apache_https.conf /etc/apache2/sites-available/apache_https.conf
# RUN a2ensite apache_https
# RUN a2ensite apache_redirect_to_ssl.conf
# RUN systemctl restart apache2
# EXPOSE 443

### STAGE 2: Run with Apache2 and HTTPS ###
# FROM httpd:2.4
# RUN a2enmod proxy
# RUN a2enmod proxy_http
# COPY apache_http.conf /etc/apache2/sites-available/apache_http.conf
# RUN a2ensite apache_http
# RUN systemctl restart apache2
# EXPOSE 80